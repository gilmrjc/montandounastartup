module Middleman
  module MetaTags
    module Helpers
      def meta_tags_image_url(source)
        meta_tags_host + current_resource.url + image_path(source)
      end
    end
  end
end
