module Middleman
  module Blog
    module Helpers
      def article_url(article_title, blog_name = 'posts')
        article_resource = blog(blog_name).articles.find do |article|
          article.title.downcase == article_title.downcase
        end
        article_resource.url
      rescue StandardError
        ''
      end
    end
  end
end
