xml.instruct!
xml.urlset 'xmlns' => 'http://www.sitemaps.org/schemas/sitemap/0.9' do
  resources = sitemap.resources.select do |page|
    page.path =~ /\.html/ &&
      !page.data.noindex == true && page.path !~ /blog/
  end
  resources.each do |page|
    xml.url do
      xml.loc "#{data.site.host}#{page.url}"
      lastmod = if page.data.modified
                  page.data.modified.to_time.iso8601
                else
                  Date.today.to_time.iso8601
                end
      xml.lastmod lastmod
      xml.changefreq page.data.changefreq || 'monthly'
      xml.priority page.data.priority || '0.5'
    end
  end
end
