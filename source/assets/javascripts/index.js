function enableSiteMenu () {
  const menu = document.getElementById('site-menu')
  const navbar = document.getElementById('site-navbar')

  menu.addEventListener('click', (e) => {
    e.preventDefault()
    navbar.classList.toggle('header__navbar--visible')
  })
}

document.addEventListener('DOMContentLoaded', () => {
  enableSiteMenu()
})
