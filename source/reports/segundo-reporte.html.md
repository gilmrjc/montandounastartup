---

title: Segundo reporte
date: 2020-02-27
modified: 2020-02-27
initial: 2020-01-11
end: 2020-02-27
description: >-
  Este reporte será breve porque no le he dedicado mucho tiempo al sitio. Es
  normal que perdamos la constancia en un proyecto, no hay nada malo en ello. Lo
  importante es reconocerlo y tratar de continuar.
pull_image: report-hero.jpg

---

Este reporte será breve porque no le he dedicado mucho tiempo al sitio.
Es normal que perdamos la constancia en un proyecto, no hay nada malo en ello.
Lo importante es reconocerlo y tratar de continuar.

Durante estos dos primeros meses del año sucedieron cosas en mi vida personal y
laboral que consumieron mi tiempo.
Debido a estas situaciones, perdí el ritmo de publicaciones.
Escribo esto para mantener la promesa de documentar cada acción tomada, o no
tomada, respecto al proyecto.

Al ser fin de mes, este es el mejor momento para escribir un reporte y retomar
el paso.
Manteniendo el formato, incluyo los [datos](#datos) relevantes en este período y
unas [conclusiones finales](#conclusiones-finales) sobre el estado del proyecto
y las siguientes metas.

## Actividades

Como lo mencione en la entrada, durante este mes no hice nada público.
La principal razón por la que tuve que pausar el proyecto fue que entré a un
nuevo trabajo.
Ahora estoy apoyando a una empresa a transicionar de un esquema tradicional a
uno digital.
Esto está relacionado con este proyecto, ya que me permite trasladar algunas de
mis ideas a la práctica y verificar mis hipótesis sobre como se debe de
gestionar una startup.

Cerrando este pequeño paréntesis, hubo un par de actividades que realice este
mes, por lo que tampoco se puede dar por perdido.
Entre las cosas que llegue a realizar están:

- [Comenzar un podcast](#comenzar-un-podcast)
- [Actualizar el logo](#actualizar-el-logo)
- [Expandir mi red de apoyo](#expandir-mi-red-de-apoyo)

## Comenzar un podcast

Tengo un par de entrevistas grabadas con dos amigos donde hablamos de
emprendimiento y gestión de empresas desde el punto de vista de un CTO.
Aunque ya tengo las grabaciones, me hace falta editarlas y publicarlas.
Seguramente lo haré el siguiente mes.

Comenzar un podcast me permite expandir el tipo de contenido generado y tener
opiniones de otras personas, lo cual es muy enriquecedor.
Me parece una gran forma de exponer temas que desconozco o donde una persona más
experta puede aportar otro punto de vista.
Por esta razón he decidido probar el formato de podcast con entrevistas
mensuales.

## Actualizar el logo

Como parte de la planeación del podcast, recibí apoyo para la generación de
algunos recursos visuales.
Entre estos recursos se encuentra el logo.

El logo que tenía hasta este momento era una simple imagen que encontré en
Internet.
Aunque no es necesario contar con una marca desde el primer día, tenerla
siempre es una ventaja.
Contar con un logo propio es un paso hacia la creación de una identidad y
marca, lo cual es necesario en el largo plazo.

## Expandir mi red de apoyo

Durante el mes de Enero tuve conmigo, de forma momentánea, a un pequeño
equipo que me ayudó a redactar un par de artículos.
De la misma forma que deje pausado este proyecto, también se pauso esa
colaboración.
Sin embargo platiqué con algunas otras personas que también encontraron
interesante esta idea y con las que espero estar colaborando activamente en los
siguientes meses.

Algunas veces es dificil encontrar a alguien que confíe en tus ideas, y uno
nunca tiene demasiada ayuda.
Por eso es importante expandir tu red de apoyo todo lo posible, aún cuando no
parezca necesario.

---

## Datos

En esta sección del reporte se muestran los datos relevantes para poder llevar
un seguimiento contínuo y observar la evolución de los diferentes indicadores.

![Visitas sitio web](segundo-reporte/Visitas-10-02-27.png)

En la imagen se puede observar la comparación entre el mes de Enero y el mes de
Febrero.
Esta de más decir que Febrero fue un mes practicamente muerto.
A pesar de que hubo un par de visitas, en general se puede observar como fue
decayendo hasta terminar en ceros la última semana.

Esto deja muy claro la importancia de mantener un ritmo de publicaciones
constante.

El resto de indicadores que medí durante el mes de Diciembre se comportan
basicamente de la misma manera.
Una tendencia a la baja durante el mes de Enero para terminar en ceros durante
Febrero.
No incluyo las gráficas porque considero que es espacio que puede ser mejor
aprovechado.

---

## Conclusiones finales

Al ser este un proyecto nuevo y con poco contenido, detener la publicación de
material tiene grandes consecuencias.
Mantener un blog, y en general cualquier cosa que tratemos de comenzar, requiere
mucho esfuerzo.
No siempre es posible conservar el enfoque, sobre todo cuando nuestras
actividades del día a día se vuelven demandantes.

Es importante encontrar la manera de equilibrar nuestro trabajo y vida personal
con nuestros proyectos secundarios.
Cuando fallamos en hacerlo, debemos detenernos a reflexionar porque sucedió y
que podemos hacer para evitarlo.
En algunas ocasiones la respuesta es dar nuestro proyecto por terminado.
En otras es ver este fallo como un tropiezo, levantarnos y continuar el camino.

Por eso la conclusión de este reporte es que aquí seguimos, sacudiendonos un
poco el polvo y dando los primeros pasos después de una pequeña caida.
Recordemos que emprender es más parecido a un maratón que a una carrera de 100
metros.
Lo importante no es ser los más rápidos, sino mantenernos en movimiento para
lograr llegar a la meta.
