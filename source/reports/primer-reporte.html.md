---

title: Primer reporte
date: 2020-01-12
modified: 2019-01-12
initial: 2019-12-01
end: 2020-01-10
description: >-
  Los reportes tienen como objetivo documentar todas las acciones que no se
  explican detalladamente en otra parte del sitio. Este primer reporte explica
  las acciones iniciales que se tomaron para iniciar el proyecto.
pull_image: reports-hero.jpg

---

Los reportes tienen como objetivo documentar todas las acciones que no se
explican detalladamente en otra parte del sitio.
Este primer reporte explica las actividades iniciales que se tomaron para iniciar
el proyecto.
De esta manera se puede seguir paso a paso cada una de las acciones realizadas
durante la gestión del proyecto.

También se incluyen los [datos](#datos) relevantes en este período y unas
[conclusiones finales](#conclusiones-finales) sobre el estado del proyecto y las
siguientes metas.

## Actividades

Durante las primeras semanas de vida de este proyecto se realizaron las
siguientes actividades:

- [Comenzar a escribir artículos](#comenzar-a-escribir-articulos)
- [Crear el sitio web](#crear-el-sitio-web)
  - Comprar dominio
  - Configurar DNS
  - Publicar blog
  - Configurar correo electrónico
- [Pedir opiniones de forma directa](#pedir-opiniones-de-forma-directa)
- [Pedir opiniones en LinkedIn](#pedir-opiniones-en-linkedin)
- [Herramientas digitales](#herramientas-digitales)
  - [Implementar analytics](#implementar-analytics)
    - Matomo
    - Tag manager
    - Metrica
    - Google Analytics
  - [Activar servicio de Google Search
    Console](#activar-servicio-de-google-search-console)
  - [Implementar Disqus](#implementar-disqus)
  - [Crear cuenta en Moosend](#crear-cuenta-en-moosend)
- [Establecer la estrategia de contenidos][estrategia-contenidos]
- [Mejorar el diseño](#mejorar-el-diseo)
- [Implementar Woopra](#implementar-woopra)
- [Conseguir equipo de apoyo](#conseguir-equipo-de-apoyo)
- [Primer campaña LinkedIn](#primer-campaa-linkedin)
- [Configurar primer objetivo del sitio](#configurar-primer-objetivo-del-sitio)
- [Primeras entrevistas con prospectos](#primeras-entrevistas-con-prospectos)
- [Primer demostración de servicios](#primer-demostracin-de-servicios)

## Comenzar a escribir articulos

El propósito inicial de este proyecto era crear una guía que explicara paso a
paso como iniciar un negocio.
Como parte de este proyecto, y para tener ejemplo real, decidí añadir el
servicio de consultoría para emprendedores.
Así el nuevo objetivo es servir como apoyo a cualquier persona que trate de
iniciar una startup y el modelo de negocios se basa en ofrecer servicios
personalizados de asesorías y mentorías.

Para cumplir con la primera parte, comence a documentar [mis pensamientos sobre
el emprendimiento][hola-mundo] y la importancia de tener [una idea
clara][sentido].
La intención de escribirlos era poder plasmar mi punto de vista y usar eso como
referencia para quien quisiera entender de que trata todo esto.

## Crear el sitio web

Para poder recibir opiniones de los primeros artículos, opté por publicarlos en
forma de blog.
De esta manera sería muy fácil poder compartirlos y las personas que lo leyeran
podrían tener una forma sencilla de volver a visitarlos.

Este paso también [fue documentando][sitio-web] al mismo tiempo que se
ejecutaba.
Dentro de las cosas que realice al crear el sitio web están la compra del
dominio, la configuración necesaria para publicar el blog, el sistema del blog
en si mismo y dar de alta el servicio de correo electrónico.

Con todo esto listo, me dispuse a pedir opiniones a amigos y conocidos para
tratar de entender si este proyecto era relevante para alguien que no fuera yo.

## Pedir opiniones de forma directa

Las primeras opiniones sobre este proyecto fueron realizadas por persona a las
que me acerque de forma directa.
La intención era conocer su punto de vista sobre los temas y si les gustaba el
formato.
Sus comentarios fueron positivos y me pidieron que pasará a temas más avanzados,
lo cual valido mi teoría de que la gente quiere tener una guía práctica y
detallada para transformar una idea en un negocio.

Uno de ellos comentó que la narración le parecia interesante y un recurso
diferente a lo que habia leído en otros blogs de emprendimiento, por lo que ese
estilo seguirá siendo uno de los principales en este sitio, y la razón de este
reporte.

## Pedir opiniones en LinkedIn

La segunda ronda de opiniones fueron solicitadas en LinkedIn, donde escribí una
publicación pidiendo a cualquiera que lo viera a hacerme comentarios sobre el
sitio.
Esto, de nuevo, era para validar el interés sobre el tema.

La razón para pedir comentarios en una red social se centraba en pasar de mis
conocidos a personas que no conociera y evitar el sesgo que eso implicaba.
Es probable que las primeras personas opinarán que les agradó la idea para no
hacerme sentir mal, y no por la idea en si misma.
El tener opiniones de otras personas daba un resultado más objetivo.

Las opiniones que recibí durante esta prueba fueron diferentes a lo que
esperaba.
Aunque un par de ellas continuaron validando mi hipótesis, la gran mayoría se
enfoco en el nulo diseño que tenía en ese momento el sitio y me recomendaban
mejorarlo.

La cantidad de comentarios que recibí criticando el diseño en lugar de el
contenido me hizó detenerme en este punto y plantearme la mejora del sitio.
Sin embargo, las pocas respuestas que si se enfocaban en el contenido fueron
suficiente para considerarlo como algo positivo.

## Herramientas digitales

De forma paralela a las opiniones continue trabajando en el sitio.
Una de las primeras cosas que hice fue decidir que herramientas y servicios
utilizar como apoyo.
Debido a que esto se realizó al mismo tiempo que las acciones anteriores, la
implementación y configuración de estas herramientas se hizo antes de mejorar
el sitio, que fue una conclusión posterior.

El primer grupo de servicios que contemple fueron los de analítica, que
permiten medir la cantidad de personas que visitan el sitio y el comportamiento
de las visitantes.
El segundo tipo de servicios son los de subscripción de correo para crear una
audiencia y fidelizar a los interesados.
El último servicio, que no es una categoría por si mismo, es el que provee
Google para examinar el comportamiento de un sitio respecto a las busquedas que
realiza la gente.

### Implementar analytics

Para tener un control sobre la cantidad de personas que visitaban el sitio y
poder ver el desarrollo del mismo implementé un sistema de analíticas.
El sistema más común es el de Google Analytics, sin embargo para este caso
quería tener mayor control sobre los datos desde el inicio.
Para lograr esto la plataforma elegida fue [Matomo].

Matomo es un sistema de analítica de código abierto que puede correr en
servidores propios, igual que la base de datos que resguarda la información.
Una de las herramientas incluidas es un tag manager, que es un gestor de
códigos de rastreo.
Con este tag manager configuré el servicio de Matomo, de Google Analytics y de
[Metrica].
Este último es proveido por Yandex, la alternativa rusa a Google, que ofrece
capacidades interesantes como la grabación de visitas y mapas de calor de forma
gratuita.

Estas cuatro herramientas integran el sistema de analítica inicial.
Normalmente solo se utiliza Google Analytics, pero como siempre pasa, entre más
fuentes de información y puntos de contacto existan es más fácil hacer
comparaciones y tomar decisiones.
Además al ser gratuitas no representan un costo extra que cubrir.
Esto nos da todos los beneficios sin ninguna desventaja.

### Activar servicio de Google Search Console

Una de las herramientas que proporciona Google es la Search Console.
Esta consola nos da información sobre el rendimiento de nuestro sitio dentro del
buscador y nos permite detectar que búsquedas generan tráfico hacia nosotros.
Tener esta información es muy importante ya que podemos ver cuanta gente
encuentra nuestro contenido y en que posición nos encontramos para cada uno de
los términos en lo que aparecemos.
También podemos ver que páginas se encuentran indexadas, si existe algún error
y con que frecuencia nos visita el Bot de Google.
La activación se puede hacer de distintas maneras, las más comunes son al
validar la propiedad con un registro DNS o por una vinculación con la cuenta de
Analytics instalada en la web.

### Implementar Disqus

Como la fase inicial incluía la recopilación de comentarios, un paso natural fue
la configuración de Disqus en el sitio.
[Disqus][Disqus] es un servicio externo que permite a las personas realizar
comentarios en una página web.
Normalmente el sistema de gestión de contenidos ya posee está habilidad, pero
incluso personas que usan Wordpress instalan Disqus en su sitio.

Las ventajas que provee son varias, pero la más importante es que ellos
gestionan la cuenta del usuario, por lo que nosotros no guardamos esa
información.
Otra ventaja es que al tener un perfil unificado para comentar en cualquier
sitio, Disqus puede recomendar contenido relevante para el lector, y esta web
podría aparecer en esas recomendaciones aún cuando nunca haya sido visitado.

Delegar la función de comentarios a un servicio externo tiene un inconveniente y
es que esa información ya no es nuestra.
Sin embargo los beneficios superan esa desventaja por el momento y ese es un
problema menos que atender, por esa razón este sitio usa Disqus.

### Crear cuenta en Moosend

Para las listas de correo decidí usar [Moosend][Moosend].
Aunque la herramienta más popular en este espacio es MailChimp, la decisión se
basa por una parte en el precio y por otra parte en las funcionalidades.

Moosend proporciona todos los servicios (excepto el de Landing Page) de forma
gratuita para cualquier persona que tenga menos de 1,000 contactas.
Esto es atractivo considerando que todos los demás proveedores manejan planeas,
donde la capa gratuita tiene muchas restricciones.
Además de esto, los diferentes planes que ofrece son más baratos que planes
similares, por lo que es una buena decisión inicial para mantener los costos
bajos.

En lo que se refiere a las funcionalidades, tiene un gestor de plantillas para
correo, automatización, segmentación y servicios de analítica.
No tiene nada que los otros sistemas no tengan, pero para la fase inicial esto
es más que suficiente para empezar a trabajar.
Si en algún momento del futuro se requiere algo más específico o funciones
especiales es posible exportar la lista de contactos y probar con otra
plataforma.

## Mejorar el diseño

Como se mencionó anteriormente, los primeros comentarios se referian de forma
más frecuente al diseño que al contenido.
Por esta razón se mejoró el diseño inicial como parte de una [estrategia para
mejorar la confianza][estrategia-confianza] en el sitio.
El sitio funcionó mucho mejor con este cambio ya que de forma inmediata, solo
un día después, una persona se suscribió a la lista de correo, lo cual no era
uno de los objetivos pero demuestra que existe confianza para proporcionar
datos personales.

## Implementar Woopra

Con el nuevo diseño publicado, se agregó una herramienta de analítica extra.
[Woopra][woopra] es una herramienta cuyo principal objetivo es analizar de forma
más sencilla.
A diferencia de los otros servicios ya instalados, la interfaz de Woopra se
siente más amigable.
Otro de los puntos fuertes es la automatización de acciones en base al
comportamiento de los usuarios.
Aunque aún no he usado esta parte de la herramienta, es un bueno saber que
tenemos este tipo de funciones disponibles.

Al igual que todas las otras herramientas instaladas, Woopra ofrece el servicio
de forma gratuita dentro de un limite razonable.
Hasta este momento mi filosofía es que no hay ninguna razón para no probar una
herramienta cuando no nos genera un costo.

## Conseguir equipo de apoyo

Durante la última semana de Diciembre platique con un amigo sobre este
proyecto.
A él le intereso mucho y nos pusimos de acuerdo para empezar a trabajar de forma
conjunta.
Llegamos al acuerdo de asociarnos y durante la fase inicial, en la que no hay
ingresos, el me prestara de forma parcial a su equipo de trabajo mientras yo le
ayudo a mejorar su negocio.
Esto se mantendrá hasta lograr generar ingresos y tengamos que cambiar este
esquema.
Así es como el equipo paso de ser solo una persona a ser 5.

Las ventajas de tener un equipo es que hay más personas colaborando para el
mismo fin y todos los objetivos parecen lograrse más rápido.
Las desventajas de tener un equipo más grande es que requiere de un esfuerzo de
comunicación y coordinación que hace parecer que los objetivos son más
dificiles.
Ya veremos que ocurre pero es una buena excusa para empezar a introducir temas
de gestión de equipos en este blog.

## Primer campaña LinkedIn

Para probar si el rediseño mejorará realmente la interacción de las personas con
el sitio se creó una campaña en LinkedIn.
El objetivo de esta campaña era promocionar las publicaciones que ya estaban
dentro de la página y observar que pasaba con las personas que llegaban al sitio
a traves de publicidad.

Para hacer esta prueba el presupuesto invertido fue de $50 dólares durante un
periodo de 5 días.
Los resultados fueron positivos ya que hubo varias personas interesadas que se
registraron en la lista de correos, lo cual se puede considerar como un
resultado positivo.

## Configurar primer objetivo del sitio

Para llevar control de la campaña se configuraron las distintas herramientas de
analítica para registrar la suscripción como un objetivo del sitio.
De forma simple, los objetivos en la analítica son las acciones que esperaramos
de parte de los usuarios cuando interactuan con nuestro contenido y se miden
como un indicador de éxito del mismo.

## Primeras entrevistas con prospectos

Para tratar de entender como se puede monetizar el servicio de asesorías a
emprendedores se entrevisto a varias personas que tienen o tuvieron un proyecto
para preguntarles como les gustaría recibir apoyo.
Las entrevistas se realizaron bajo un modelo de desarrollo de clientes, el cual
se documentará de forma más detallada después.

Las conclusiones a las que se llegaron con estas entrevistas es que si existe
interés de parte de los emprendedores para recibir asesoría y que si están
dispuestos a pagar por un servicio.
Varios de los problemas que mencionaron tener encajan dentro de la filosofía de
este proyecto y mencionaron las desventajas que tiene soluciones similares como
las incubadoras o aceleradoras.

Aunque esta información no es suficiente para tomar desiciones importantes como
el precio o el enfoque de ventas, sirve para entender mejor cuales son sus
problemas, cuales son los problemas de la competencia en este mercado y entender
cuales son nuestras ventajas frente a las opciones que existen hoy en día.

## Primer demostración de servicios

En la segunda semana de Enero mi nuevo socio me presento con uno de sus
clientes.
La idea era ofrecerle este servicio y ver cual era su reacción al hacer una
demostración.
El perfil de este cliente corresponde a una persona con estudios en
administración de empresas con un negocio tradicional.

La actividad que hicimos fue un análisis y modelado financiero de su negocio
para ayudarle a entender mejor las consecuencias de una inversión fuerte que
tenía planeada para este año.
Al inicio de la demostración se mostro interesado pero nos comentó que él
también había realizado un análisis con anterioridad.
Mientras realizabamos este ejercicio nos confirmo que ambos teniamos
conclusiones similares.
Hasta ese punto no había nada nuevo para él en la mesa.

El punto de inflexión fue cuando se aplicaron algunos conceptos que son muy
comúnes en las finanzas de una startup pero no en los negocios tradicionales,
como el cálculo del CAC (Costo de adquisición).
Al entender el costo que representaba cada cliente en base a la cantidad de
dinero asignada a marketing logramos crear un plan que minimizaba las perdidas
del año.
Con este modelo también pudo observar como afectan las ventas de cada producto
al ingreso.

Después de crear varios escenarios con diferentes presupuesto de marketing y
estrategias de ventas por producto paso de una proyección en la cual recuperaba
su inversión en 5 años a una donde necesitaba invertir aproximadamente un 20%
extra pero el tiempo de recuperación se reducia a 8 meses.

Con esta nueva información cambio su estrategia de ventas del año.
Al final quedo muy contento con el servicio y la información que le
proporcionamos de su propio negocio.
El único punto negativo es que aunque le mencionamos que podiamos ayudarlo a
crear un plan de acción para ejecutar de forma eficiente estos cambios, nos
mencionó que tenia toda la información necesaria para hacerlo él mismo.

---

## Datos

En esta sección del reporte se muestran los datos relevantes para poder llevar
un seguimiento contínuo y observar la evolución de los diferentes indicadores.

![Visitas sitio web](primer-reporte/Visitas-10-01-20.png)

El primer dato importante a revisar es el comportamiento del tráfico dentro del
sitio.
Como se puede observar en la gráfica, los primeros días fueron muy activos y
coinciden con las primeras impresiones recopiladas.
Después de este pico las visitas cayeron y empezaron a crecer de forma gradual.
Esta segunda fase corresponde con el rediseño del sitio y la difusión vía
LinkedIn, tanto de forma orgánica como de forma pagada.

![Impresiones LinkedIn](primer-reporte/Impresiones-LinkedIn-10-01-20.png)

El canal inicial por el que se comenzó a promover el contenido fue LinkedIn
debido al enfoque de esta red social.
Para impulsar el reconocimiento de la marca y tratar de entender a personas de
una forma más amplia se creó un campaña publicitaria.
Esta incremento el número de impresiones sobre el contenido como se puede
observar en la gráfica.

![Interacciones LinkedIn](primer-reporte/Interacciones-LinkedIn-10-01-20.png)
![Seguidores LinkedIn](primer-reporte/Seguidores-LinkedIn-10-01-20.png)

Como resultado de esta exposición, muchas personas empezaron a interactuar con
el contenido y seguir la página en LinkedIn.
Lo interesante es ver que el nivel de interacción orgánico es 5 veces superior
a la interacción producto del contenido patrocinado.
Esto una vez más valida que las personas que conectaron con la idea realmente
tienen interés sobre los temas.

![Subscriptores mailing](primer-reporte/Mailing-10-01-20.png)

Otro dato interesante que muestra el interés de forma más directa es la lista de
correo.
Durante los 5 días que duró el la pauta publicitaria en LinkedIn 4 personas
dejaron su correo electrónico, lo cual es algo inesperado, ya que esperaba que
nadie lo hiciera hasta quizá después de un mes.
Los otros 2 lo hicieron antes de que se creará la campaña y el correo removido
es mi propio correo que utilize para probar la configuración del sistema.

---

## Conclusiones finales

El primer mes y medio del proyecto ha sido muy interesante, incluso para mi.
Todo Diciembre fue dedicado a construir el sitio y comenzar a plasmar las ideas
iniciales.
En términos del rendimiento para las publicaciones, fueron muy malos días ya que
coincidio con el período de vacaciones y fin de año, por lo que no muchas
personas estaban pendientes del Internet.
Sin embargo tener este tiempo disponible fue bueno para sentar las bases y
dedicar el tiempo necesario a todo lo indicado en este resúmen.

De la misma forma la primera semana de Enero siguio la misma inercia del
comienzo del año.
Lo interesante es que a partir del día 5 de Enero todo empezó a mejorar.
Esto lo interpreto como un gran comienzo de año y de aquí en adelante solo resta
seguir con el impulso inicial para continuar creciendo.

Lo más importante en este periodo para mi fue la demostración que hicimos.
Al terminar la persona no tenía intención de contratar nuestros servicios.
Nos dio el consejo de no resolver el problema más importante en la primera
sesión para que el cliente tenga un incentivo para continuar con las asesorías.
Fue un resultado agridulce ya que aunque coincido con esa postura, el objetivo
era realizar una demostración y no una venta.
El aprendizaje que nos dejo esa sesión es que realmente somos capaces de mejorar
un negocio en un par de horas y para mi eso es más valioso que una venta.


[hola-mundo]: posts/hola-mundo
[sentido]: posts/el-sentido-de-la-vida-el-universo-y-todo-lo-demas
[sitio-web]: posts/un-lugar-en-el-espacio
[estrategia-contenidos]: posts/creando-una-estrategia-de-contenidos
[estrategia-confianza]: strategies/mejorar-la-web
[matomo]: https://www.matomo.org/
[metrica]: https://metrica.yandex.com/
[disqus]: https://disqus.com/
[moosend]: https://moosend.com/
[woopra]: https://www.woopra.com/
