site = data[:site]

feed_name = site[:site]
site_url = site[:host]
site_description = site[:description]

articles = []
articles.concat(blog('posts').articles)
articles.concat(blog('strategies').articles)
articles.concat(blog('reports').articles)

articles.sort! { |a, b| b.date <=> a.date }

xml.instruct!
xml.rss :version => '2.0', 'xmlns:media' => 'http://search.yahoo.com/mrss/' do
  xml.channel do
    xml.title feed_name
    xml.link site_url
    xml.description site_description
    xml.atom :link,
             href: URI.join(site_url, current_page.path),
             rel: 'self',
             type: 'application/rss+xml'

    xml.pubDate articles.first.data.date.to_time.rfc822
    xml.lastBuildDate Date.today.to_time.rfc822

    articles[0..10].each do |article|
      xml.item do
        xml.title article.title
        xml.link URI.join(site_url, article.url)
        xml.guid URI.join(site_url, article.url)
        xml.tag! 'media:content', url: "#{site_url}#{article.data.cover}"
        xml.pubDate article.data.date.to_time.rfc822
        xml.description article.data.description
      end
    end
  end
end
