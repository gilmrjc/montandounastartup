---

title: Un lugar en el espacio
description: >-
  Para que está guía sea realmente útil es necesario que sea accesible para
  todos en el Internet. En está entrada explicaré como pase de un par de
  documentos en la computadora a este blog. Hay tanto una explicación técnica
  como una no técnica, para que tú tambien puedas comenzar a tener un lugar en
  la red.
date: 2019-12-11
modified: 2019-12-11
cover: /posts/un-lugar-en-el-espacio/cover.jpg
pull_image: un-lugar-en-el-espacio/cover.jpg

---

En los artículos anteriores comencé a explicar que uno de los objetivos de este
proyecto es documentar los procesos necesarios para crear un negocio a partir
de una idea, sin embargo para que esta documentación sea útil debe ser pública
y accesible en Internet.
Este proyecto inicio en mi computadora como un par de borradores, por lo que
antes de continuar analizando los siguientes pasos en este viaje, quiero empezar
a compartir lo que he escrito hasta ahora y esto implica crear un blog, o un
sitio web, para publicar la información.

En mi caso particular quiero trabajar con un conjunto de herramientas con las
que me siento cómodo y puedo explotar más adelante por lo que el proceso que
elegí es muy técnico, sin embargo también explicare como lograr l mismo de
forma más sencilla.
Es importante considerar que el nivel de flexibilidad depende de los objetivos a
cumplir y la capacidad técnica disponible.
Mi recomendación general es priorizar los objetivos sobre la capacidad técnica,
ya que el iniciar un proyecto así puede parecer divertido para alguien técnico
pero terminará consumiendo más tiempo del necesario y será difícil de manejar
para el resto del equipo.

## Versión técnica

Para cualquiera en el área de tecnología el proceso es simple.
Todo lo que hay que hacer para comenzar es comprar un dominio, configurar el
DNS para apuntar a los servidores y montar el CMS elegido.
Si esto no te suena como algo simple a pesar de tener un perfil técnico, no te
preocupes, este proceso solo se realiza una vez al iniciar el proyecto y puede
ser que no te haya tocado realizarlo.
Lo que yo hice, paso a paso, fue lo siguiente:

**Comprar un dominio**: Una herramienta que puede ayudar a tomar esta decisión
es [domcomp], el cual es un comparador de precios.
Uno de los detalles a considerar es que esta empresa tiene convenios con algunos
proveedores para poder ofrecer precios de compra más bajos pero con un costo de
renovación alto, por eso es importante revisar la letra pequeña.
Al abrir la lista completa de proveedores se muestra información adicional y ahí
es donde mencionan si es una promoción o si se incluye algún servicio extra.
El proveedor que yo utilizo es [namecheap] que aunque no es el más económico, a
pesar del nombre, tiene un sistema de administración muy sencillo y es fácil
cambiar las herramientas adicionales que provee, lo cual no ocurre con otros
proveedores.

**Configurar el DNS**: Una vez que tenemos el dominio necesitamos configurar
los registros DNS, que traducen las direcciones web en las direcciones IP de
los servidores, para que se pueda acceder a nuestro sistema o plataforma.
En este caso utilizare [CloudFlare], el cual ofrece muchas ventajas dentro del
plan gratuito.
A pesar de que se pueden comprar dominios directamente ahí, utilizo NameCheap
porque CloudFlare prohíbe el uso de servidores de DNS externos.
Para configurarlo, lo único que se debe hacer es crear una cuenta, importar la
información del dominio y comenzar la migración.
Todos los pasos se explican al iniciar sesión por primera vez.
El único detalle que varía, dependiendo donde se compro el dominio, es la
migración.
En el caso de NameCheap solo se debe de entrar en la administración del dominio,
buscar el apartado de **NAMESERVERS** y elegir la opción de DNS personalizado.
Una vez seleccionado, se ingresa la información proporcionada por CloudFlare y
se guardan los cambios para terminar la migración.

**Hosting**: Para desarrollar el sitio utilizaré un SSG (Static Site
Generator), por lo que no es necesario tener un servidor.
El código fuente estará hospedado en [GitLab], que permite [alojar sitios
estáticos](https://about.gitlab.com/product/pages/) de forma gratuita, de esta
manera puedo manejar el proyecto y publicar el sitio con la misma herramienta
sin ningún costo.
Al ser un desarrollo propio, esta táctica representa más trabajo que utilizar
algún CMS popular pero me permite ahorrar costos de infraestructura y conservar
la flexibilidad que quiero.

## Versión no técnica

Como sugerencia inicial recomendaría estudiar un poco los conceptos básicos
como que es un dominio, un servidor DNS y un hosting, ya que siempre es bueno
tener una idea general de lo que conlleva montar un sitio web.
Sin embargo el proceso descrito anteriormente sigue siendo muy complejo para
montar un sitio web.
Una forma más sencilla y directa es contratar un servicio de hosting que se
ofrece en la mayoría de proveedores de dominios.
Para un blog la opción más popular es una instalación gestionada de Wordpress,
aunque en lo personal recomendaría usar [Ghost] como plataforma porque es más
fácil de usar y contiene más funciones integradas.
Si el objetivo de este sitio no es generar contenido sino validar la idea, se
puede comenzar con una landing page usando alguno de los siguientes recursos:

- [Lander](https://landerapp.com/)
- [Launchaco](https://www.launchaco.com/)
- [Carrd](https://carrd.co/)
- [Leadpages](https://www.leadpages.net/)

De esta forma se puede crear un sitio web o un blog en cuestión de horas y
oficialmente existir en el Internet.

## Bola extra - El email

Uno de los servicios que no contraté a la hora de comprar el dominio fue el
correo electrónico.
La opción más común para es contratar [G Suite], que es la versión privada de
Gmail e incluye calendario, espacio de almacenamiento en la nube.
Una alternativa menos conocida pero mejor en términos costo/beneficio es
[Zoho Workplace].
Esta plataforma incluye más herramientas que G Suite y es gratuita para los
primeros 5 usuarios, además a la mitad del costo una vez que se rebasa este
limite.
En otro post explicaré a detalle como aprovechar estas herramientas pero entre
los extras que incluye se encuentra Cliq, una alternativa a Slack, una suite de
ofimática, un generador de formularios y encuestas, espacio en la nube y
streams, una herramienta que no tiene una alternativa clara pero que resulta muy
útil.
La configuración tanto de G Suite como de Zoho requiere hacer cambios en los
registros DNS, lo cual es algo que si va a requerir el apoyo de una persona que
tenga conocimientos de estos temas pero en ambos viene muy bien explicado por lo
que no debe ser difícil empezar a usarlos.

Ahora si, con un sitio en línea y el correo electrónico habilitado, ya podemos
declararnos habitantes del mundo digital.
A partir de ahora todos los pasos que siga para levantar este negocio serán
públicos y siempre podrán enviar un correo a contacto@montandounastartup.com
para ponerse en contacto.
¡Que empieza la aventura!

[domcomp]: https://www.domcomp.com
[namecheap]: https://www.namecheap.com
[cloudflare]: https://www.cloudflare.com
[gitlab]: https://www.gitlab.com
[ghost]: https://ghost.org/
[g suite]: https://gsuite.google.com.mx
[zoho workplace]: https://www.zoho.com/workplace/
