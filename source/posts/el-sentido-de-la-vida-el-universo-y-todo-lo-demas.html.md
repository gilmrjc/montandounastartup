---

title: El sentido de la vida, el universo y todo lo demás
description: >-
  Antes de emprender un negocio debemos verificar que nuestra idea tiene
  potencial. Para ello debemos expresar de forma clara y concisa su propuesta
  de valor. Una forma fácil para pasar de una idea a una propuesta de valor es
  determinar su utilidad práctica con dos simples preguntas: ¿para qué? y ¿por
  qué?
date: 2019-12-03
modified: 2019-12-03
cover: /posts/el-sentido-de-la-vida-el-universo-y-todo-lo-demas/cover.jpg
pull_image: el-sentido-de-la-vida-el-universo-y-todo-lo-demas/cover.jpg

---

Lo primero que se debe hacer para emprender es tener una idea.
No necesariamente debemos partir de un problema, puede ser simplemente algo que
queramos hacer, por ejemplo, quiero desarrollar una aplicación que me permita
registrar mis recorridos cuando viajo para tener organizadas mis aventuras por
el mundo.
Siempre podemos formalizar y refinar nuestros objetivos, pero sin ideas no
podemos hacer nada.

Una vez que encontramos el punto de partida debemos preguntarnos ¿para qué? y
¿por qué? Eso nos va a permitir crear un proyecto alrededor de la idea.
Siguiendo con el ejemplo anterior, construir esa aplicación sería conveniente
**para** organizar la información sobre mis viajes, hacer recomendaciones a
otros viajeros o crear una lista de lugares que quiero conocer.
Contar con una herramienta que me ayude con estas tareas es útil **porque**
hace que no dependa de mi memoria y sea mucho más sencillo buscar, organizar,
preservar y compartir esas experiencias.
Responder estas dos preguntas hace fácil de detectar la propuesta de valor y los
objetivos a cumplir en la fase inicial.

Para aplicar esta dinámica a este proyecto en particular lo primero que debo es
preguntar ¿para qué y por qué comenzar una consultora para emprendedores? Antes
de responder, hay que considerar que existe una trampa en la que podemos caer a
la hora de contestar.
Mi primer intento ilustrará lo que quiero decir: para mi comenzar una consultora
es importante porque quiero compartir mi conocimiento con los demás y me gusta
participar de forma activa en los proyectos en los que me involucro.
El error en la explicación anterior es que no responde ¿para qué?  sino ¿para
quien?, en este caso yo, y el por que de la idea es una justificación personal.

La meta de este ejercicio es entender mejor la idea para **establecer los
objetivos iniciales** y una forma de comprobar que lo estamos haciendo bien es
compartir el resultado con alguien que vaya a usarlo y/o beneficiarse para ver
si logra identificarse.
En el primer caso es muy probable que si le explicamos el por que y para que de
la aplicación a un viajero su reacción sea "yo también quiero algo así" mientras
que en el segundo caso es menos probable.

Una vez aclarado esto trataré de explicar las razones detrás de mi idea.
Una consultora para emprendedores sirve para acceder a mucha información de
manera personalizada, armar una ruta crítica que permita desarrollar mis
proyectos, compartir experiencias para contrastar mi situación actual y tener
un espacio en el cual puedo poner a prueba mis creencias y teorías porque
cuando uno empieza se enfrenta a varios retos por primera vez, no sabe como
armar un plan de forma integral, desconoce las herramientas, técnicas y métodos
existentes y necesita apoyo para decidir cual es el momento y forma ideal para
tomar y ejecutar una decisión.

Ahora que explique la motivación de este proyecto la pregunta más importante de
todas es: como lector interesado en emprender ¿te identificas y te estás
diciendo a ti mismo que quieres algo así?
