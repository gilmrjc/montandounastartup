---

title: Las 3 C de una startup
description: >-
  Todas las startups tienen algo llamado el C-Level. Algunas preguntas
  importante que nos podemos hacer son ¿cuántas personas del C-Level se
  necesitan para comenzar? ¿quiénes son estas personas? ¿cuáles son sus
  funciones? En este post te explicamos cual es el C-Level ideal para una
  startup.
date: 2020-03-1
modified: 2020-03-01
cover: /posts/las-3-c-de-una-startup/cover.png
pull_image: las-3-c-de-una-startup/cover.png

---

Todas las startups tienen algo llamado el *C-Level*.
Este grupo de personas conforman lo que en español se denomina como *la
directiva* de la empresa.
Esta forma de llamarles se debe a que, en inglés, el director de un área es
conocido como **Chief Something Officer**, donde *Something* es el área que
lideran.
Por ejemplo, los más comúnes son el CMO (Chief Marketing Officer), el COO (Chief
Operating Officer) y el CTO (Chief Technology Officer).

La gran mayoría de veces estos puestos son ocupados por los fundadores, aunque
no es raro que se contrate a una persona externa para cubrir uno de estos roles.
Tampoco es extraño que con el paso del tiempo los fundadores sean remplazados
por alguien con más experiencia en el cargo.

Algunas preguntas importante que nos podemos hacer son
¿cuántas personas del C-Level se necesitan para comenzar?
¿quiénes son estas personas?
¿cuáles son sus funciones?

Lo ideal es contar con 3 miembros iniciales: el CEO, el COO y el CTO.
Cada uno de ellos tiene un papel muy importante y sus contribuciones son
necesarias para garantizar el buen funcionamiento del negocio.
Es posible empezar sin alguno de ellos pero como veremos, hacerlo reducirá
nuestras probabilidades de éxito.

Cuando los fundadores tienen mucha experiencia en el funcionamiento del negocio
y del mercado, es posible que el CEO tengan un doble rol y haga las veces del
COO.
El problema con este esquema no es la falta de habilidades sino la falta de
tiempo.
Aunque las personas a cargo sepan muy bien que hacer, la cantidad de actividades
a desarrollar en una startup va a hacer que tengan un desempeño deficiente por
el poco tiempo disponible para cada una.
Por eso, aunque es posible iniciar con solo dos personas, yo recomiendo comenzar
con tres.

Ahora veamos cuales son las funciones que desempeña cada persona:

## El CEO (Chief Executive Officer)

El CEO es el director ejecutivo.
Una forma muy simplista de verlo es como el jefe de todos, o al menos así es en
el mundo corporativo tradicional.
En el caso de las startups, cuando al comienzo solo existe el C-Level, todos
pertenecen al mismo nivel jerárquico por lo que el CEO no es el jefe de los
demás.

Debido a que su rol es más bien de liderazgo, el CEO es quien tiene la última
palabra, aunque no la primera.
Esto significa que debe existir suficiente autonomía para que cada integrante
del equipo tome sus propias decisiones y el CEO debe ser consultado como último
recurso, cuando no esté claro cual es la mejor opción y sea necesario tomar una
decisión.

Sin la confianza plena entre todos, es complejo hacer cualquier tipo de avance
significativo, por lo que el CEO debe adoptar una postura de apoyo hacia los
demás en todo momento para fomentar un ambiente de confianza y respeto
generalizado.
Esto lo separa completamente de la figura del jefe poderoso que estamos
acostumbrados a ver en las grandes corporaciones.

Ahora ¿quién debe ser el CEO de la startup? Antes de responder esta pregunta es
importante entender que para una gran empresa un CEO necesita tener una gran
cantidad de habilidades empresariales como conocimientos en administración de
empresas, gestión de equipos, finanzas, desarrollo de estrategias de negocios
etc.
Sin embargo, en el contexto de una startup, la persona que ocupe este puesto no
necesita tener un perfil profesional específico y practicamente cualquiera puede
dirigir su propia empresa.

El CEO debe ser la persona que ha detectado una oportunidad de negocio, quien
ha encontrado un problema en su entorno inmediato tan molesto que justifique el
pagar por una solución.
La razón por la cual el CEO debe ser quien que encontró el problema tiene que
ver con su rol como lider.
**El trabajo del CEO no es resolver el problema, su trabajo es convencer a los
demás que vale la pena resolverlo**.
El resto del equipo si tiene como responsabilidad ofrecer soluciones.
Si el CEO no entiende de forma personal el problema a resolver, no importa
cuales sean sus habilidades, dificilmente será capaz de liderar a un equipo que
trabaje en el mismo.
Entre sus responsabilidades están explicar el problema, describir las molestias
de quienes lo tienen y verificar si las soluciones propuestas realmente lo
resuelven.

Otras responsabilidades del CEO incluyen:

- Reclutar, contratar y retener el mejor talento para construir al equipo de
    trabajo.
- Encargarse de que haya suficiente dinero en la caja para poder seguir
    operando, tanto por la captación de inversión como por la creación de los
    presupuestos.
- Crear y mantener viva la cultura de empresa, estableciendo los valores
    fundamentales que todos deben de compartir.
- Remover los obstáculos que el equipo encuentre, actuando como un facilitador
    en todo momento.

Todas las responsabilidades anteriores son importantes, pero establecer y
comunicar la misión de la empresa es la más importante.
Hacerlo permite que el resto del equipo entienda mejor los objetivos y, de esta
manera, cada quien pueda complementarlo con sus habilidades para lograr realizar
con éxito todas las tareas necesarias y llevar a buen puerto el proyecto.

## El COO (Chief Operation Officer)

El COO es el director operativo.
En las empresas tradicionales, el COO se encarga de gestionar los procesos
internos de la empresa, incluidas las finanzas, las operaciones comerciales y la
gestión del equipo.
Este rol puede lucir similar al del CEO, y lo es en una empresa tradicional.

La diferencia entre ambos radica en el tipo de procesos que cada uno debe
manejar.
Mientras el CEO de una startup se centra en los procesos que tienen que ver con
el equipo, el COO se encarga de los procesos relativos a la actividad
comercial.
Una vez que la empresa crece y el CEO cambia de prioridades, el COO absorbe sus
responsabilidades para asumir el rol que normalmente tendría.

En un principio el tipo de procesos que tiene a su cargo incluyen:

- Manejar al equipo de ventas
- Revisar los procesos logísticos internos
- Coordinar los procesos logísticos externos
- Gestionar la relación con los proveedores

Muchas de estas actividades son necesarias para que el negocio pueda funcionar,
de ahí el porque es el encargado de las operaciones.
La principal función del COO es ser la mano derecha del director general y
asegurarse que mientras él está ayudando al equipo, la compañia continua
trabajando.

Además de ser capaz de realizar estas tareas, **lo ideal es que el COO sea
experto en el giro del negocio**, ya que al estar al frente de las operaciones
debe solucionar cualquier problema que aparezca.
Si la empresa se dedica al campo de la salud, el COO debería tener experiencia
en la gestión de instituciones médicas.
Si la empresa se dedica al entretenimiento, el COO debería tener experiencia en
el desarrollo de proyectos artísticos.

Recordemos que el CEO no es necesariamente experto en el tema y que el COO es
su mano derecha, por lo que será la primera persona a la cual recurrir cuando
exista alguna duda o problema.
Tener un COO con poca experiencia en el área del negocio puede hacer que las
operaciones sean mal ejecutadas, con las consecuencias que ello implica.

## El CTO (Chief Technology Officer)

El CTO es el director de tecnología.
Normalmente se encarga de establecer la estrategía tecnológica de la empresa,
esto es, hacer propuestas sobre el uso de las distintas tecnologías
disponibles, la estructura de los equipos de trabajo y supervisar el plan de
desarrollo para garantizar la eficiencia operativa y presupuestal de esta área.

Para una startup el CTO tiene un rol muy distinto, ya que no existen equipos que
gestionar ni certezas que permitan crear un plan de desarrollo a largo plazo.
En la primera fase del emprendimiento el CTO es la única persona que pertenece
al equipo de tecnología, por lo que gran parte de su trabajo consistirá en el
desarrollo del MVP inicial.

Además de realizar las actividades propias de cualquier programador, el CTO debe
ser capaz de autogestionarse, crear un plan de trabajo, explorar los distintos
puntos de vista sobre el problema y crear la primera versión del producto.
Esto lo convierte en su propio jefe, ya que el resto del equipo no tiene la
capacidad de verificar si lo que está haciendo es correcto.
Esto también impide que los demás colaboren con él de forma directa, por lo que
la interacción queda reducida a la presentación de avances y la realización de
demostraciones.

Durante el desarrollo del MVP el CTO no solamente es responsable de la
tecnología, también es el encargado del desarrollo de producto.
Aunque esto parezca ser lo mismo, las tareas adicionales que esto representa
incluyen:

- Generar propuestas de solución al problema.
- Desarrollar el plan de trabajo.
- Contratar al resto del equipo.
- Gestionar la infraestructura técnologica.
- Verificar la viabilidad de las soluciones propuestas.
- Coordinar al equipo de diseño y el equipo de desarrollo.
- Brindar soporte técnico a los usuarios.
- Implementar las sugerencias y solicitudes de los clientes.
- Delimitar el alcance de cada uno de los proyectos.

Conforme el equipo va creciendo, el CTO cambia de una posición de desarrollo a
una de gestión de equipos, normalmente entre el mes 6 y el mes 18.
Cuando esto ocurre el CTO debe tener a su disposición un equipo integrado por
diseñadores, gestores de producto y desarrolladores.
**Su principal función sigue siendo la de liderar el desarrollo del producto**,
aunque cada vez comienza a delegar más funciones al resto del equipo.
Cuando el negocio a crecido lo suficiente como para tener más de una linea de
negocio, por ejemplo, el desarrollo de una aplicación web y una aplicación
móvil, o la gestión de dos procesos distintos dentro del mismo sistema, el CTO
migra a una posición estratégica.
En este punto cada equipo debe ser capaz de gestionar por si mismo la rama de
negocio a la cual pertenece y el CTO se encarga de mantenerlos alineados al
revisar los planes de trabajo y sugerir formas de cooperación entre ellos.

## Ejemplo

Imaginemos un caso muy simple para demostrar como interactuan los fundadores de
una startup y cuales son sus diversas funciones.

David es un profesionista que está casado con Luisa, que también es
profesionista.
Ambos tienen una carrera exitosa y continuan escalando posiciones en sus
trabajos.
A pesar de estar en un gran momento de sus vidas, David tiene una enorme
preocupación: el estar tan ocupados en el trabajo les impide ir por sus hijos a
la escuela.
David y Luisa contrataron un servicio de transporte escolar para solucionar este
problema, ya que no quieren que su hijo regrese solo a casa.

En un día de descanso, David decide ir él mismo por su hijo a la escuela.
Cuando llega se da cuenta que se dirige al transporte escolar, por lo que lo
detiene antes de que lo aborde.
Estando ahí se da cuenta de la cantidad de niños que son transportados, hace
cuentas rapidamente en su cabeza y se da cuenta del gran negocio que representa.

Hay cosas que a David le gustaría cambiar del servicio, y piensa que si lo hace
podría ofrecer un mejor servicio para el resto de los padres.
Entre las cosas que quisiera tener están:

- Una aplicación que le permita ver la ubicación de su hijo en tiempo real.
- Visibilidad sobre la hora de salida de la escuela y de llegada a su casa.
- Capacidad de pago con tarjeta de crédito.

Con esta idea en la cabeza, David comienza a pensar en las personas que podrían
formar parte del equipo.
Dos le vienen a la mente de inmediato.

En primer lugar esta Manuel, un amigo de la preparatorio que estudio psicología
en la universidad.
En un principio parece una decisión extraña, pero David sabe que Manuel trabaja
en el departamento de recursos humanos de una maquiladora, y entre sus muchas
responsabilidades está la de gestionar el transporte del personal.
Manuel ha aprendido mucho sobre logística y transporte, ya que debe asegurar que
el personal siempre llegue a tiempo a la empresa sin importar lo que pase.
Eso incluye verificar el estado de los equipos, las rutas de transporte,
anticipar si habrá cualquier tipo de contratiempo como manifestaciones o mal
clima.
Por esta razón Manuel es la persona indicada para ayudarlo a realizar este
proyecto.

La segunda persona a la que invita es a Edgar, un amigo del trabajo que está
en el departamento de sistemas.
David saber que Edgar quiere independizarse y trabajar en su propia empresa pero
no tiene ningún tipo de formación administrativa que le ayude a realizarlo.
Edgar es el encargado de un equipo de desarrollo y al escuchar la idea queda
fascinado, ya que el tiene el mismo problema en casa.
Sin dudarlo, le ofrece toda su ayuda a David para crear la aplicación que
necesita.

Durante el primer mes, los tres se ponen de acuerdo durante los fines de semana
en como funcionará la empresa.
El acuerdo al que llegan es el siguiente:

David será el CEO.
Su tarea será encontrar los colegios cercanos a la escuela de su hijo y
recabar información sobre los servicios de transporte que se ofrecen, como parte
de la investigación de mercado.
Su misión es entender como funcionan los distintos servicios, cuales son los
precios y si existe algún tipo de vinculo directo entre los transportistas y
las instituciones educativas.

Por su parte Manuel, ahora el COO, se pone a buscar vehículos que les permitan
comenzar.
Deciden comprar una camioneta usada para no tener que invertir mucho dinero,
pero deben asegurarse que se encuentra en perfectas condiciones y que se puede
acondicionar para el transporte de los niños.
Con la información que David le proporciona, comienza a estudiar el
comportamiento del tráfico en las colonias aledañas para tratar de crear
posibles rutas.
También contacta con un par de choferes que conoce para negociar un contrato y
cubrir las plazas que tienen contempladas.

Edgar, quien es el CTO, se dedica durante los primeros dos meses a desarrollar
la aplicación.
Para hacerlo, invita a un amigo diseñador para prototipar la primera versión.
Con un par de pantallas diseñadas, David comienza a preguntarle a otros padres
de familia si les interesaría el servicio.
Con la validación inicial de la idea, Edgar trabaja durante todas las tardes
hasta que, en una de las presentaciones semanales, los tres deciden que es
tiempo de salir al mercado.

David vuelve con los padres de familia para decirles que están listos para
ofrecer el servicio, por lo que recibe los primeros pagos.
Manuel utiliza las direcciones que se registran en el sistema para crear las
rutas y durante las primeras semanas acompaña al conductor para cersiorarse que
todo marcha con normalidad.
Edgar comienza a recibir los primeros informes de errores y continua trabajando
para solucionar estos problemas, aunque está en el proceso de contratar a
alguien que se encargue de esto para seguir añadiendo funcionalidades a la
aplicación.

De esta forma el equipo inicial, conformado por David, Manuel y Edgar, logran
emprender.
El éxito aún no es algo seguro, pero cada uno de ellos tiene muy claro cual es
su rol en el equipo y como pueden contribuir para que las cosas funcionen.

---

Despues de explicar los distintos puestos que un equipo inicial debe cubrir
¿tienes a alguien en mente que pueda ayudarte a emprender? Si ya has comenzado
la aventura del emprendimiento ¿hay alguna puesto que se necesite redefinir o
cubrir?
