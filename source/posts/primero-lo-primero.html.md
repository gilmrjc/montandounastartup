---

title: Primero, lo primero
description: >-
  El problema más común que sufre cualquier emprendedor es decidir que hacer.
  Para resolverlo debemos tener en claro cual es la principal tarea de una
  startup: minimizar riesgos. Una técnica que estudiaremos, útil para determinar
  como minimizarlos, es el mapa de problemas y soluciones.
date: 2019-12-18
modified: 2019-12-18
cover: /posts/primero-lo-primero/cover.jpg
pull_image: primero-lo-primero/cover.jpg

---

Ya tengo una idea, ya verifique que a la gente le interesa y ¿ahora qué? Antes
de responder esa pregunta es necesario entender cual es la tarea más importante
que debe resolver alguien que inicia una startup.
En mi caso, a mi siempre me dijeron que lo más importante para un negocio es
encontrar un cliente, por que sin clientes un proyecto no es un negocio.
Durante mi primer intento, para mi lo más importante era construir el sistema
porque sin sistema no había nada que ofrecer a los posibles clientes.
Ninguna de las dos era lo más importante.
Cada quién tiene ciertas ideas preconcebidas o algo de intuición que le dice
por donde empezar y usamos eso como punto de partida pero ¿cómo podemos
decidir correctamente que debemos hacer? Esa es la pregunta del millón de
dólares.
La verdad es que no existe una respuesta infalible, lo que podemos hacer es
tratar de acercanos lo más posible y esa es la tarea más importante de cualquier
persona que intenta emprender: decidir que es lo que se debe de hacer en cada
momento.

Por supuesto, esto parece más retórica barata que algo útil.
Todos saben que decidir es algo importante pero la pregunta sigue sin respuesta:
**¿cómo sé si estoy tomando las decisiones correctas?** Es una pregunta justa
la cual tiene una respuesta sencilla, aunque no se debe confundir sencillo con
fácil.
Antes de responder revisemos los problemas que enfrentamos.
El principal reto al que se enfrenta una startup es a sobrevivir.
Una mala decisión puede hacer que el negocio cierre.
La razón más común es la falta de dinero, pero esa solo es una consecuencia.
Las verdaderas razones son una mala planeación financiera, no cubrir los
objetivos de ventas, no terminar el producto en tiempo o incumplir algunas
promesas realizadas a los clientes.
Afortunadamente al comenzar el dinero no es el mayor problema, o al menos no
debería serlo.
Un gran consejo es comenzar con los suficientes recursos como para funcionar al
menos 3 meses, o un año en caso de conseguir inversion, sin necesidad de
realizar una venta.
El segundo reto es el crecimiento.
Para crecer se requiere armar el equipo de trabajo, hacer que la gente se
entere del producto, conseguir los primeros clientes y definir el plan de
trabajo, entre otras tantas cosas.
En resumen, el principal reto al que se enfrenta una startup es: crecer sin
morir en el intento.

Ahora que está claro cual es el reto común de todas las startup, podemos
responder la pregunta.
**Lo que debemos hacer es tomar decisiones que nos ayuden a resolver cualquier
cosa que pueda limitar nuestro crecimiento o la operación del negocio**.
Es más fácil decirlo que hacerlo, por eso es una solución sencilla pero difícil.
Lo que podemos preguntarnos inmediatamente después de esta respuesta es ¿cómo se
qué va a limitar el crecimiento o la operación de mi negocio? Aquí es donde todo
empieza a ser "depende".
Cada empresa tiene sus propios retos y depende del producto, el equipo, el
mercado en el que se compite.
Hay muchos factores a considerar que hacen imposible tener una guía general,
pero parte de este proyecto es ejemplificar el proceso de toma de decisiones
para tener una referencia a la mano.

Una forma de expresar lo mismo que me gusta mucho es la siguiente: **La misión
de una startup es minimizar riesgos**.
Esta explicación es muy accionable.
Lo que debemos hacer es encontrar los posibles riesgos que enfrentamos y
minimizarlos.
Para esto yo hago un mapa de problemas y soluciones.
Primero analizaremos los problemas.

![Mapa de problemas](primero-lo-primero/problemas.jpg)

En este tablero podemos observar los problemas que he identificado como bloqueos
para el crecimiento y operación de este proyecto.
La forma en la que están ordenados es de acuerdo al beneficio y riesgo.
Por un lado el beneficio me indica que tan importante es algo en base a lo que
me va a generar de retorno, mientras que el riesgo me dice que tan necesario es
minimizar ese problema.
Viendo este mapa, la reacción inicial puede ser un poco de confusión.
La parte más riesgosa es verde mientras que la parte menos riesgosa está en
rojo.
**Nuestro sentido común nos dice que eso esta mal, lo más riesgoso debe ir en
rojo precisamente porque es riesgoso, pero este mapa tiene otro objetivo**.
Esta no es una lista de actividades, en cuyo caso efectivamente es preferible
hacer primero lo menos riesgoso, es una lista de problemas en donde tenemos que
identificar amenazas y atacar las que más nos pueden afectar.
Como ya he estado trabajando hay algunos avances.
Inicialmente solo existían un par de notas: "nadie entiende mi idea" y "a nadie
le interesa mi idea".
Este es el tema con el que abrí el blog, explicar el para que y porque de una
idea, ya que si no somos capaces de explicar que queremos hacer dificilmente
podremos continuar con lo demás.

Una vez expresado que es lo que quiero hacer y cual es su utilidad, asi como
verificado que otras personas se identifican y comparten un interes, puedo
continuar con el mapa.
Un problema en el que me voy a enfocar es la confianza, que las personas vean
este sitio como un lugar confiable para informarse, porque mi objetivo es que
cualquier emprendedor tome esto como un apoyo y lo vea como su primera opción.
Dos variables que determinan la confianza son el contenido y el reconocimiento.
Como puede verse en el mapa, yo priorice el crear artículos y mejorar el blog
sobre tener reconocimiento.
Esto es porque es más facil crear una audiencia a partir del contenido que crear
una reputación sin el.
En caso de privilegiar el reconocimiento sobre el contenido probablemente
tendría que trabajar en alguna empresa grande, o crear una, para utilizarlo como
referencia y acumuluar muchos años de experiencia que me respalden.
Ninguno enfoque es mejor que el otro, y el camino que se quiere tomar depende
enteramente de quien lo va a recorrer.
Este pequeño mapa es una muestra de como pienso priorizar mis actividades y
cualquiera puede hacer lo mismo con su propio proyecto.

Ahora que conocemos los problemas y como están organizados, podemos pasar al
mapa de soluciones.

![Mapa de soluciones](primero-lo-primero/soluciones.jpg)

Las soluciones son un espejo del mapa de problemas.
Cada problema tiene una o más posibles soluciones ubicadas en el mismo lugar
para poder comparar ambos documentos facilmente.
Lo que a mi me funciona es tomar las tareas en diagonal, como lo marca el mapa
de problemas, y empezar por lo más importante cuando pertenecen al mismo nivel.
Como se puede ver en la imagen anterior el siguiente paso a resolver es crear un
plan para el contenido y mejorar la estructura y diseño del blog.
Después de esto empezaré a promover el blog activamente y en este punto, ya con
una audiencia que pueda darme retroalimentación, volveré a modificar mi mapa.
**Recordemos que nuestra tarea principal no es seguir un plan sino construir el
plan**, por lo que este documento debe trabajarse de forma continua y revisarse
cada vez que hayamos resuelto un par de problemas o detectemos uno nuevo.
Así siempre estaremos seguros que estamos tomando las decisiones correctas en el
momento adecuado.

Para poder ver mucho más claro estos documentos, y tal vez copiarlos, dejaré la
herramienta con la que construí ambos mapas abajo.

<iframe
  width="640"
  height="360"
  src="https://miro.com/app/embed/o9J_kvuz-xE=/?"
  frameborder="0"
  scrolling="no"
  allowfullscreen
></iframe>
<br />
