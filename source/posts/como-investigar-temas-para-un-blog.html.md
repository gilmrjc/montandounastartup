---

title: ¿Cómo investigar temas para un blog?
description: >-
  Escribir un blog es muy simple, escribir para que sea visitado no tanto. Lo
  que hace la diferencia es como investigamos los temas a publicar. Una
  metodología que permita crear un plan de redacción y hacer un análisis
  competitivo es lo que separa a los que tienen tráfico en sus webs de los que
  no lo tienen.
date: 2019-12-26
modified: 2019-12-26
cover: /posts/como-investigar-temas-para-un-blog/cover.jpg
pull_image: como-investigar-temas-para-un-blog/cover.jpg

---

El primer tema que abordaré en este blog es que es una startup.
Para poder escribir sobre el tema es necesario conocerlo, pero también se debe
tener un plan para su redacción.
Así que veamos como investigar este tema.

## Investigación de palabras clave

En primer lugar trataremos de entender la importancia del tema.
Para esto usaremos [Ubersuggest], que es una herramienta para analizar palabras
clave y sitios web.
Algunos datos relevantes que podemos encontrar son el número de busquedas que se
realizan sobre el término, la dificultad para competir dentro de Google y las
páginas que se encuentran como primeros resultados.

![Analizando
keywords](como-investigar-temas-para-un-blog/analizando-keywords.png)

Entre más busquedas tenga un tema mejor será para nosotros, ya que eso es una
señal de interés y nos permite calcula la cantidad de personas que podrían
encontrarnos.
Si los términos clave que estamos tratando de cubrir no tienen un volumen grande
es mejor pensar en cambiar de tema o adaptar el enfoque para asegurar un mejor
desempeño de nuestro contenido.

Además del término clave central, en las sugerencias podemos encontrar palabras
clave relacionadas y sus busquedas estimadas cada mes.
Estas palabras las agregaremos a nuestro plan de redacción para tratar de
incluirlas dentro del artículo.
Esto nos ayudará a posicionarnos dentro de Google por esos términos alternativos
y aumentar las probabilidades de que alguien lo encuentre como resultado de una
búsqueda.

## Estudios competitivos

Otro tipo de información que tenemos disponible es la lista de los primeros
resultados de búsqueda.
Uno de los usos más directos de esta lista es como referencia.
Podemos leerlos para entender porque han funcionado.
Sin embargo también podemos analizarlos para entender su valor desde la
perspectiva del SEO.
Para esto recurrire a [Serpstats], una herramienta que nos permite hacer este
tipo de análisis de forma gratuita.

![Análisis competitivo de palabras
clave](como-investigar-temas-para-un-blog/analisis-competitivo-de-palabras-clave.png)

La información que nos proporciona son las palabras clave a las que responde ese
artículo y su rendimiento.
Estos nuevos términos podemos considerarlos como nuevas sugerencias a agregar a
nuestro plan de redacción.
Otra ventaja es que al leerlo podremos tener esta información a la mano y tratar
de mejorarlo.

![Terminos alternativos de la
competencia](como-investigar-temas-para-un-blog/terminos-alternativos-competencia.png)

Incluso podemos encontrar sugerencias como la lista de palabras clave faltantes,
que son las palabras clave que incluye la competencia pero no se encuentran en
este contenido.
Con toda esta información podemos crear un plan de redacción sólido.

## Creando el artículo

Una regla que ya no funciona para mejorar el posicionamiento de una página es
incrementar la densidad de palabras clave, esto es, repetir una y otra la vez la
misma frase para intentar subir posiciones en el buscador.
Esto se debe a que ahora los buscadores hacen interpretaciones semánticas y
tratan de revisar el contenido como si fueran un ser humano.
La consecuencia de este cambio es que, al igual que una persona no entendería un
texto con tantas repeticiones y le daría una mala calificación, un buscador web
se da cuenta de este tipo de estrategias tramposas y penaliza el contenido.
Por ello es importante contar con un buen número de términos alternativos y
sinónimos dentro de la publicación.

![Resultado de la
investigación](como-investigar-temas-para-un-blog/resultado-investigacion.png)

Para tener esta información a la mano, podemos consolidar nuestra investigación
en un hoja de cálculo electrónica ([aquí esta la
mía](https://sheet.zoho.com/sheet/published.do?rid=i90lx17816c3d828c4aeaaf2b4535359a58a4)).
Dentro de esta hoja podremos ver que palabras clave investigamos, en que fecha
lo hicimos, cuales son los términos alternativos, quien es la competencia en
este espacio, y yo incluyo también las publicaciones relevantes en el idioma
inglés.
Con estos datos tengo todo lo necesario para comenzar a escribir un nuevo
artículo.

Para finalizar esta entrada, quiero resaltar que el nuevo comportamiento de los
motores de busquedas nos incentiva a crear artículos pensados en las personas.
Debemos tratar de exponer los temas de la forma más amplia pero sencilla
posible.
Con nuestra investigación nos aseguramos de tener los elementos necesarios para
crear un gran contenido.
Una vez que terminamos esta fase, lo mejor que podemos hacer es compartir
nuestro conocimiento y aportar algo único y diferente a lo ya existente para
diferenciarnos del resto.
Lo que realmente nos va a ayudar es eso, agregar algo nuevo a todo el
conocimiento preexistente.

[ubersuggest]: https://app.neilpatel.com/
[serpstats]: https://serpstat.com/
