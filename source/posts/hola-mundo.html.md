---

title: Hola mundo
description: >-
  Hoy en día puedes aprender casi cualquier cosa en Internet. Basta con buscar
  ¿cómo puedo ...? para encontrar algo útil. Una de las excepciones es ¿cómo
  montar una startup? No existe una guía simple y directa para esta pregunta.
  Por esa razón se inició este proyecto.
date: 2019-12-01
modified: 2019-12-01
cover: /posts/hola-mundo/cover.jpg
pull_image: hola-mundo/cover.jpg

---

Hoy en día uno puede aprender lo que sea en Internet.
Basta con buscar "¿Cómo programar?" para tener a nuestra disposición millones
de resultados en múltiples formatos, cursos gratis o de paga, en inglés o
español, por escrito o en vídeo.
Así es como aprendí yo.
Después de una simple búsqueda encontré lo necesario para empezar en el mundo
del desarrollo y sin darme cuenta pase de un "Hola Mundo", el ejercicio más
básico que existe, a diseñar sistemas cada vez más complejos.
Descubrí un lugar que no tenía límites, donde era capaz de hacer todo lo que
imaginará, donde tenía control.
Me sentía muy cómodo en ese espacio y de repente llegó la inspiración.
¿Por qué no hacer algo útil con todo ese conocimiento?

Aprendí a programar porque tuve la fortuna de acceder al Internet desde mi
adolescencia, pero durante años no fue más que un simple pasatiempos.
Igual que quien aprende a tocar la guitarra sabiendo que no se convertirá en
músico profesional no había ningún uso real que yo pudiera darle, hasta que me
llegó una idea, mi primer intento de emprender.
La cuestión era muy simple: si tengo un problema de salud ¿cómo puedo encontrar
al mejor especialista? Era algo muy tangible porque justo eso es lo que ocurría
en mi entorno, varios familiares tenían problemas de salud y necesitaban
encontrar a un especialista que los tratará.
Afortunadamente conocemos a muchos médicos que nos asesoraron, pero los que no
tienen esa oportunidad ¿qué pueden hacer? Así fue como se me ocurrió: si puedo
buscar casi cualquier cosa en Internet ¿por qué no puedo buscar especialistas
médicos?
No solo encontrar información sobre ellos, sino investigar si realmente son
buenos y decidir a quien debo consultar.

Hacer la primera versión de la plataforma fue sencillo, me tardé una semana en
tenerla lista, y entonces me di cuenta que efectivamente esa era la parte más
fácil.
¿Cómo hacer que la gente se enterará de lo que estaba haciendo? ¿Cómo conseguir
que los médicos se dieran de alta? ¿De donde iba a sacar el dinero para hacerlo
funcionar de forma continua? De pronto pase de una situación en donde lo tenía
todo resuelto a no tener ni idea de que debía hacer.
En ese momento ni siquiera había escuchado el termino "Startup".
Lo único que tenía claro es que encontré un problema que me afectaba de forma
directa y quería resolverlo.
Por supuesto el desenlace de la historia es muy predecible: jamás logré hacer
que mi idea funcionará.
**Transformar una idea en algo útil para la sociedad requiere más que la
habilidad de construirlo**.
Esa fue la lección que aprendí y esa se volvió una de mis metas en la vida, si
pude aprender a programar también iba a aprender lo necesario para resolver
problemas reales.

Regresando al presente, ahora se lo suficiente como para entender lo que
representa emprender y sentirme cómodo de intentarlo.
He participado en muchos proyectos y aunque no puedo calificar a ninguno como
verdaderamente exitoso (tengamos en cuenta que el tiempo promedio de un proyecto
para madurar es de 5 años y no he estado lo suficiente en ninguno) he aprendido
valiosas lecciones.
Me he involucrado desde la etapa de idea hasta la de expansión, de formar al
equipo inicial hasta llegar a un equipo consolidado, de ejecutar a dirigir, de
trabajar con emprendedores locales a equipos con personas de todo el mundo.
Estas experiencias tan diversas han hecho que recupere la sensación de control
sobre lo que hago.

## ¿Cómo montar una startup?

Gracias al Internet pude aprender a programar y eso me cambio la vida, pero aún
no puede decirme como emprender.
Si intento buscar "¿Cómo montar una startup?" no encuentro lo necesario.
Casi todo el material que he visto parece más contenido de autoayuda que una
guía funcional.
Me queda claro que cada proyecto es diferente y que dan unos lineamientos muy
generales porque no pueden entrar en los detalles pero puedo resumir miles de
artículos y presentaciones sobre emprendimiento en los siguientes pasos:

1. Busca el problema o necesidad que resuelve tu idea.
1. Encuentra el modelo de negocio en el que encaja la idea.
1. Define la propuesta de valor que te hace diferente.
1. Contrata al mejor equipo que puedas encontrar.
1. Desarrolla un MVP (Producto Mínimo Viable).
1. Valida el modelo de negocios.
1. Levanta capital para seguir creciendo.
1. Construye tu marca.
1. Establece una estrategia de salida y ejecútala.

Todo suena muy bien hasta que empezamos a preguntarnos ¿Cómo? ¿Cómo realizo
cada uno de esos pasos? Hace poco platiqué con un amigo que había llevado a
cabo esta lista de tareas y me dijo que se sentía muy confundido.
No era porque no entendiera lo que le pedían, lo que me comentó es que cuando
armo la hoja de cálculos con la proyección del negocio veía un futuro brillante,
todo indicaba éxito, pero cuando hacia a un lado la computadora no entendía como
iba a lograrlo.
¿Cómo lograr que la realidad coincidiera con su modelo? ¿Qué tenía que hacer
para alcanzar cada una de las metas que había establecido? ¿Cómo saber que no
había alguna variable escondida que él no hubiera considerado?

Aún con un plan bien diseñado que permita responder esas preguntas siguen
surgiendo dudas ¿Qué herramientas debo usar? ¿Cómo coordino al equipo? ¿Qué
técnicas me permiten recopilar la información que necesito? ¿Cómo valido mis
hipótesis? ¿Cómo se si estoy haciendo lo correcto? En definitiva no hay ningún
lugar donde puedas encontrar toda está información reunida y aprender como
montar una startup, si quieres hacerlo te tocará trabajar duro e invertir
muchos años de tu vida.
Por eso he decidido empezar este proyecto, porque quiero ayudar a que otros
aprendan esas lecciones, porque vivimos en una época en la que ya no es
necesario reinventar la rueda, porque el mundo requiere de emprendedores que
resuelvan la mayor cantidad de problemas en el menor tiempo posible.

Mi plan es el siguiente: dedicaré los siguientes meses a este blog para tener
suficiente material de apoyo y practicar la filosofía **Eat your own dog
food**, que es un término usado cuando una empresa usa su propio producto.
Documentaré los procesos que se realizan cuando se inicia una startup y una vez
que tenga lo necesario crearé una.
Haré público todo para poder mostrar un ejemplo real de como funciona.
Mi objetivo es hacer 3 cosas: ser consultor de emprendimiento para apoyar a
quien necesite ayuda en su proyecto, iniciar una startup con un modelo B2B e
iniciar una startup con un modelo B2C.
Los últimos dos me ayudarán a demostrar las diferencias en el enfoque que hay
en cada tipo de modelo comercial.
Al ser experimentos más que negocios en forma, trataré de resolver problemas
sencillos y tal ves no parezcan nada innovador.
El objetivo para mi no será que estas propuestas funcionen y se vuelvan un caso
de éxito, mas bien es ayudar a que otros emprendedores vean lo que sucede tras
bambalinas.

Por último, he dicho que mucho material sobre emprendimiento me parece contenido
de autoayuda, pero les dejaré un vídeo que en lo particular me gusta mucho
porque explica de manera muy sencilla lo que para mi significa realmente
emprender (el intro es un poco largo pero el contenido vale la pena).

<div class="center">
  <iframe
    width="560"
    height="315"
    src="https://www.youtube-nocookie.com/embed/5AuLVPKIOFE"
    frameborder="0"
    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
    allowfullscreen
  >
  </iframe>
</div>
<br />
