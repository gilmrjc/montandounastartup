---

title: Creando una estrategia de contenidos
description: >-
  Uno de los primeros pasos para crear una estrategia de contenidos es auditar
  el material existente en una web pero ¿qué pasa cuando no existe ese material?
  Para crear una estrategia de contenidos desde cero tenemos que explicar que
  es, definir sus objetivos, sus indicadores y establecer la planeación inicial.
date: 2019-12-21
modified: 2019-12-21
cover: /posts/creando-una-estrategia-de-contenidos/cover.jpg
pull_image: creando-una-estrategia-de-contenidos/cover.jpg

---

Al momento de escribir este aticulo ya expuse de que trata [este
proyecto](hola-mundo.html), como [detectar la propuesta de valor de una
idea](el-sentido-de-la-vida-el-universo-y-todo-lo-demas.html) y cual es [la
función principal de una startup](primero-lo-primero.html).
Con toda esta información a la mano logre establecer mis siguientes pasos:
crear la estrategia de contenidos y mejorar el blog, asi que eso haré.

Normalmente el primer paso que se recomienda al crear una estrategia de
contenidos es hacer una revisión del contenido existente, usar todos los datos
disponibles para determinar como es nuestra audiencia y cuales son sus
intereses, enfocarse en los canales de distribución más existosos y crear un
calendario de publicaciones.
Todos estos consejos son muy buenos pero hay un problema: nosotros no tenemos
contenido ni datos para analizar, tocará construir una estrategia de contenidos
desde cero.

## ¿Qué es una estrategia de contenidos?

Para comenzar, tenemos que definir que es una estrategia y un contenido.
Basicamente un contenido es cualquier material informativo que generemos para
interactuar con nuestra audiencia.
Esto incluye artículos de un blog, publicaciones en redes sociales, videos,
libros digitales, listas de correo, infografias, imprimibles, etc.
Pasando a la parte de estrategia, si buscamos en el diccionario el significado
encontraremos que estrategia es una "Serie de acciones muy meditadas,
encaminadas hacia un fin determinado".
Con estas deficiones podemos llegar a la conclusión de que **una estrategia de
contenidos es la planificación de una serie de contenidos con un objetivo
común**.
No hay mayor misterio al respecto.
De nuevo esta defición nos da algunas pistas sobre que necesitamos para poder
crear nuestra estrategia.
Por un lado necesitamos establecer los objetivos y la forma de medir que tan
cerca o lejos estamos de cumplirlos y por el otro lado necesitamos un plan que
nos indique que tipo de contenidos debemos crear, que temas abordar, cuando se
deben de publicar y en que medios se van distribuir.

## Establecer los objetivos

Para establer nuestros objetivos es importante pensar en bloques de tiempo.
Los objetivos pueden ser semanales, mensuales o trimestrales.
Incluso se puede hacer una combinación de estos periodos y comenzar con un
objetivo trimestral, que será dividido en varios objetivos mensuales y
semanales.
De esta forma tendremos muy claro que es lo que queremos conseguir con nuestros
contenidos.

El objetivo nos ayuda a establecer que tipo de contenido crear, con que
frecuencia y en donde publicarlo.
Si nuestro objetivo está relacionado con presencia, es importante considerar la
creación de un blog y redes sociales con publicaciones muy frecuentes.
Si el objetivo es la generación de prospectos, podemos intentar crear material
de apoyo como plantillas y descargables que se intercambian por información de
contacto y que se generan como parte integral de una campaña de marketing.
Si el objetivo es mejorar la experiencia del usuario, podemos crear guías,
tutoriales, una sección de preguntas comunes que solo se actualizan cada vez que
hay cambios importantes en nuestro producto.
Nuestros objetivos nos ayudan a entender que crear y cada cuanto hacerlo.

Una vez que hemos definido los objetivos es crucial determinar los KPIs
asociados.
Estos KPIs (key performance indicators) son los indicadores de éxito que nos
dirán si estamos cumpliendo los objetivos.
Estos indicadores pueden ser cualquier cosa que podamos medir como el número de
visitas, el número de clientes adquiridos, el número de descargas, el número de
correos electrónicos abiertos.
Cada objetivo debe tener asociado un KPI principal, que es el indicador de
éxito, y un par de KPIs secundarios, que nos ayudan a entender el comportamiento
del KPI principal.

## Como analizar KPIs

Un ejemplo de como utilizar los KPIs para analizar el rendimiento de nuestra
estrategia es el siguiente.
Supongamos que decidimos crear un cupón promocional y enviamos un correo a todos
nuestros clientes para que lo utilicen.
El KPI principal debe ser el número de compras realizadas, ya que la razón por
la cual creamos los cupones es precisamente tratar de aumentar las ventas, sin
embargo al solo tener un indicador es muy dificil entender su comportamiento.
Si el KPI crece tendremos buenas noticias pero no sabremos si su rendimiento es
el óptimo o si puede mejorar, y si no crece no podremos diagnosticar los
problemas.

Al incluir KPIs secundarios como el número de correos enviados, número de
correos abiertos, números de clicks en el cupón y número de personas que
agregan el producto en el carrito de compras podremos explicar que es lo que
ocurre.
Si vemos que se envian 1,000 correos, se abren 600 y solo 5 personas hacen
click en el cupón, es muy claro que el contenido del correo es el punto débil,
ya que la gente si abre el correo pero no usa el cupón.
Si por el contrario, el número de personas que hace click en el cupón es 100
pero dentro del sitio la cantidad de personas que agrega el producto es 10,
quizá el problema es que el descuento que ofrecemos no es tan atractivo como
habiamos pensado.

Una forma de determinar que indicadores a utilizar es construir el camino que
recorre la persona y tratar de medir cada paso.
**Las KPIs no solo indican si estamos cumpliendo el objetivo, nos cuentan la
historia de como lo logramos**.
Por esa razón los KPIs deben ser parte del recorrido, de nada nos sirve incluir
un indicador que no nos cuenta una parte de la historia.
En el ejemplo anterior pudimos incluir como un KPI secundario el número de
visitas que recibió la página del producto.
Es probable que las visitas hayan aumentado gracias al cupón pero también se
puede deber a muchos otros factores, y este dato no ayuda a explicar si el cupón
fue exitoso o no.
Por esa razón el número de visitas al producto es un mal KPI secundario.

## Comenzando desde cero

Antes de comenzar a explicar la estrategia de contendios que se utilizará en
este sitio quiero empezar por mostrar el punto de partida en donde se encuentra
y porque los consejos que piden analizar los datos existentes no se pueden
aplicar.

![Datos de Google Analytics.](creando-una-estrategia-de-contenidos/analytics.png)

En primer lugar los datos de Google Analytics muestran que el sitio es nuevo y
que he recibido aproximadamente unas 80 visitas, nada mal para la primera
semana.
Otro número, que no se muestra aquí, es que la cantidad de usuarios que han
visitado el sitio son 15.
Eso nos da una media de 5 visitas por usuario y es un número muy simple de
explicar: 1 visita al home page, 1 visita a un artículo, de regreso al home y
una visita más a otro artículo.
Cuando se explica el comportamiento de las personas, 80 visitas no parecen ser
muchas.
También se muestra que el porcentaje de rebote es del 63%, esto quiere decir que
2 de cada 3 personas que entran no se mueven a ningún otra página dentro del
sitio durante su visita.
Estos datos parecen poco alentadores, pero en el contexto de la primer semana
también resultan poco útiles ya que no hay ninguna información clara sobre el
comportamiento de los usuarios.
Debemos esperar un poco más para poder analizarlos.

![Datos de Google Search Console](creando-una-estrategia-de-contenidos/search-graph.png)

Una segunda fuente de información es Google Search Console, que nos muestra el
comportamiento de nuestro sitio en el buscador.
En la gráfica superior deberiamos ver la tendencia de cuantos clicks se han
hecho hacia nuestro sitio desde una página de busquedas, cuantos veces ha
aparecido como resultado y cual es la posición promedio que ocupa.
Al ser un sitio nuevo todos estos números están en cero.

![Términos en Google Search Console](creando-una-estrategia-de-contenidos/search-terms.png)

Además de darnos el resumen númerico de nuestro rendimiento, nos genera la lista
de términos de busqueda que nos colocaron en alguna página de resultados.
Esto es útil para detectar que cosas busca la gente y que nosotros podríamos
responder.
Al igual que las gráficas, este apartado esta vacio y no hay forma de sacar
provecho de esta información por ahora.

Así es como comienza este sitio, sin información previa para analizar o usar
como referencia.
Para construir la estrategia de contenidos en este caso es importante pensar en
el objetivo central del proyecto y usar el conocimiento previo como base.
Podemos ir corriguiendo el camino conforme pasa el tiempo y empezamos a
recolectar algunos datos.

## Armando la estrategia de contenidos

Tomando en consideración el objetivo central de este sitio y la información
relevante para los posibles lectores se puede generar la estrategia inicial
de contenidos.
Lo que se debe trabajar en esta estrategia son los tipos de contenidos ha
generar y la planeación a seguir.

### Tipos de contenidos

Hay 3 tipos de contenidos que me gustaría tener como parte de la estrategia de
contenidos:

1. Reportes de avance y resúmenes de trabajo
1. Artículos de interes general
1. Documentación interna del proyecto

El primer tipo de contenidos son publicaciones periodicas, ya sean semanales o
quincenales, donde explique que es lo que ha sucedido en torno a este proyecto.
Existen muchas decisiones difíciles de transformar en un artículo completo o
que por tiempo no puedo documentar.
Todas estas acciones terminarán en un reporte para poder tener un archivo
histórico y usarlo como referencia en algún momento del futuro.

El segundo tipo son simplemente artículos donde explique conceptos, técnicas o
metodologías que no se pueden aplicar a este proyecto, por cualquier razón que
lo impida, pero que puedan ser de interes para los lectores.
Este contenido es necesario ya que aunque el objetivo principal de este blog es
documentar como se desarrolla un negocio, el proceso es lento y hace que el
flujo de información sea ineficiente.
Agregar un par de artículos extra cada mas permite que las personas con negocios
en etapas más avanzadas encuentren información relevante o que personas que
desconocen de los temas puedan tener artículos de ayuda.

Por último está la documentación interna.
El material de este blog puede incluir artículos explicando procesos o
decisiones en tiempo real como un ejemplo práctico, sin embargo existe otro tipo
de documentación que también se agregará al sitio pero no pertenece al blog.
Esta documentación incluirá toda la información necesaria para operar el
negocio, excluyendo información de terceros y datos sensibles.
Fuera de esto, toda la información será pública para que quien visite este sitio
pueda tener un ejemplo claro de como organizar su propia información.

### Plan general

Para gestionar de forma correcta nuestras publicaciones es importante tener una
planeación.
A la hora de planear la estrategia necesitamos tener en un lugar visible los
temas que vamos a abordar, la calendarización de los mismos, un espacio donde
anotar posibles ideas para trabajar y llevar el control general de nuestros
contenidos.
Aunque existen varias herramientas creadas justamente para ese fin, una solución
simple es crear una hoja de cálculo electrónica y utilizar un formato que nos
permita concentrar toda esta información.
En el caso específico de este proyecto la hoja de cálculo contiene la
programación temática del año, el calendario de publicaciones indicando en que
parte del proceso se encuentra cada artículo, una hoja para anotar ideas y
sugerencias, y una hoja dedicada a la investigación de campo de los temas.
La plantilla se encuentra añadida en esta misma publicación y se puede revisar
con más detalle
[aquí](https://sheet.zoho.com/sheet/published.do?rid=i90lx17816c3d828c4aeaaf2b4535359a58a4).

<iframe
  width="100%"
  height="400"
  style="border:1px solid #ccc"
  frameborder="0"
  scrolling="no"
  src="https://sheet.zoho.com/sheet/published.do?rid=i90lx17816c3d828c4aeaaf2b4535359a58a4&mode=embed"
></iframe>

## Siguentes pasos

Después de revisar el estado de este sitio, establecer los objetivos y decidir
que tipo de contenidos realizar, tenemos lista nuestra estrategia de contenidos
para trabajar durante el siguiente año.
Una revisión necesaria se hará en los siguientes 45 días para realizar los
ajustes necesarios con la información que logremos recopilar durante ese tiempo.
Mientras esperamos que eso suceda, toca trabajar en mejorar el sitio web y
seguir el plan establecido.
