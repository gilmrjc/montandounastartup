---

title: "Curso de finanzas: primer modelo"
description: >-
  Para entender mejor cómo podemos modelar un negocio y el rol de nuestros
  supuestos empezaremos por algo simple: un puesto de jugos.
  Lo primero que haremos es revisar nuestra economía unitaria, que son los
  costos y ganancias de cada producto individual.
date: 2020-04-22
modified: 2020-04-22
cover: /posts/curso-de-finanzas-primer-modelo/cover.jpg
pull_image: curso-de-finanzas-primer-modelo/cover.jpg

---

Para entender mejor cómo podemos modelar un negocio y el rol de nuestros
supuestos empezaremos por algo simple: un puesto de jugos.
El ejemplo es uno con el que todos estamos familiarizados.
Durante el primer modelo que realizaremos solo se considerará la venta de jugo
de naranja.
Lo primero que haremos es revisar nuestra economía unitaria, que son los costos
y ganancias de cada producto individual.
En este caso, implica calcular cuanto cuesta producir un vaso y definir el
precio al que lo venderemos.

<iframe
  width="100%"
  height="400"
  style="border:1px solid #ccc"
  frameborder="0"
  scrolling="no"
  src="https://sheet.zoho.com/sheet/published.do?rid=keoms5172cb00de574e4dae40003c14ad3d20&mode=embed"
></iframe>

Lo que necesitamos en primer lugar es determinar los insumos necesarios, que son
naranjas, popotes y vasos desechables con tapa.
Después, desglosaremos el costo de un jugo chico, de medio litro, y uno grande,
de un litro.
Al realizar este proceso tenemos que considerar que nuestras compras son por
paquetes o por kilogramos y no por piezas, por lo que también se deben de
calcular los costos por unidad de lo que adquirimos.

En el caso de los vasos y los popotes hay dividir el precio del paquete entre el
número de piezas que contiene.
Calcular el costo del jugo, por otra parte, es más complicado, ya que compramos
las naranjas completas.
Lo que nos ayudará es pesar cuantas se necesitan para exprimir un litro.
Con este método podemos encontrar la relación que existe entre el precio de las
naranjas y el jugo.

![Supuestos iniciales](curso-de-finanzas-primer-modelo/supuestos-iniciales.png)

Una vez realizados estos cálculos se establece la cantidad a usar de material.
Al sumar todos los costos encontramos el valor de producción de un vaso de
jugo.
Si también establecemos el precio de venta en este punto, es fácil calcular
cuanto ganamos por cada producto individual.
Solo es necesario restarle a las ganancias el costo.

![Economía unitaria](curso-de-finanzas-primer-modelo/economia-unitaria.png)

Antes de continuar explicaremos se estructuró el documento.
Una buena práctica a seguir es diferenciar los datos que se ingresan de forma
manual y los que se calculan.
Esto se puede conseguir de distintas maneras, ya sea usando diferentes colores
de letra o de fondo.
En este caso, el sistema es el siguiente: fondo amarillo para referencias
directas que no realizan un cálculo, en azul las operaciones matemáticas y en
verde los resúmenes, todos los demás son entradas manuales.
Con un código así es fácil distinguir entre cada tipo de dato a simple vista y
simplifica la búsqueda de información.

Con los costos y precios unitarios podemos elaborar varias proyecciones.
La primera que realizaremos es la de ventas.
Al tener dos productos, debemos conocer cómo se comporta cada uno de forma
individual antes de calcular los totales.
En este reporte incluiremos el precio de venta, la cantidad y costo de lo
vendido, y nuestra utilidad bruta, que no contempla los gastos adicionales como
los sueldos y la renta del puesto.
Al considerar un solo producto debe coincidir con nuestro análisis de economía
unitaria.

![Desglose ventas](curso-de-finanzas-primer-modelo/desglose-ventas.png)

Una vez que tenemos el reporte individual podemos incluir los totales.
Lo que buscamos es determinar como contribuye cada producto al conjunto
completo.
Dentro de este resumen global indicaremos las cantidades que se venden en un
mes.
Por esta razón, en la sección de desglose la cantidad aparece en color amarillo,
ya el valor editable pertenece a la tabla principal.

![Resumen ventas](curso-de-finanzas-primer-modelo/resumen-ventas.png)

Todos estos datos se encuentran juntos en la hoja de ventas.
En un solo lugar podemos observar como se comporta el año entero, cuáles son los
costos de producción y las ganancias.
En este modelo aún faltan muchas cosas, ya que no estamos contemplando, por
ejemplo, el salario del trabajador que se encuentra en el puesto de jugos.

![Reporte ventas](curso-de-finanzas-primer-modelo/reporte-ventas.png)

Tampoco coinciden los gastos reales con los proyectados porque nosotros
compramos los vasos por paquete aunque solo vendamos un jugo durante todo el
año.
A pesar de estas deficiencias podemos entender que sucede con nuestro negocio.
Con este modelo tan simple es posible jugar con los números para observar cómo
cambia nuestra ganancia dependiendo del volumen de ventas.

El siguiente paso es determinar los gastos que realizamos al comprar la materia
prima.
Al igual que en el caso anterior, empezaremos calculando el costo de cada uno de
los insumos.
La única diferencia es que también debemos considerar la cantidad de piezas
totales que compramos de acuerdo con el número de paquetes adquiridos.
Conocer esta información nos permitirá después llevar un seguimiento de
inventarios.

![Desglose compras](curso-de-finanzas-primer-modelo/desglose-compras.png)

El resumen de la hoja de compras incluye el gasto total y la cantidad de piezas
adquiridas por cada insumo.
Igual que en el cálculo de ventas, si solo compramos un paquete de materia prima
debe coincidir con nuestra economía unitaria.
Esta información proviene de la sección de supuestos iniciales.
En el caso de la naranja, al comprar por kilos asignamos de forma directa el
valor.
La razón por la cual registramos el peso y no el número de frutas es porque sin
importar el tamaño de las naranjas, siempre podemos estimar cuanto jugo
obtendremos de un kilo.

![Resumen compras](curso-de-finanzas-primer-modelo/resumen-compras.png)

Una vez que conocemos las compras y ventas se requiere un control de
inventarios.
Esto nos permite mantener una conexión con la realidad.
En los supuestos iniciales definimos que para obtener un litro de jugo se
necesitan casi 2 kilos de naranja, sin embargo en nuestro modelo vendimos más
del que podíamos producir, ya que solo compramos un kilo.

Llevar un control de inventarios nos permite entender si contamos con todo el
material necesario para cubrir lo que estamos proyectando.
Cualquier cambio en el volumen de compras y ventas afecta la cantidad de
insumos requeridos.
Con esta información disponible nos aseguramos de tener la capacidad de
producción.

Su implementación requiere desglosar la cantidad de materiales que ocupa cada
producto, que a su vez lo multiplicamos por lo que se vendió.
Así, obtenemos la materia prima que se transformó durante la producción.
En este análisis veremos que varios productos pueden ocupar el mismo insumo, por
lo que agregaremos una tabla con el total que se utilizó para producir todo.

![Desglose inventarios](curso-de-finanzas-primer-modelo/desglose-inventarios.png)

En cuanto a las compras, ya tenemos la cantidad de piezas adquiridas.
Lo único pendiente es combinar ambos datos en su propia sección.
Esta información conforma nuestro control de inventarios.
Gracias al código de colores podemos notar que toda la hoja está automatizada.
No existen campos editables porque el inventario depende de los cálculos que
realicemos en los otros reportes.

![Resumen inventarios](curso-de-finanzas-primer-modelo/resumen-inventarios.png)

En el caso analizado, donde solo hemos vendido un producto de cada tipo, la
cantidad de material a utilizar, otra vez, debe coincidir con la economía
unitaria.
Podemos revisar si nuestros supuestos se mantienen de forma consistente a lo
largo de todos los cálculos.
Si ambos datos no coinciden, es buen momento para buscar el error antes de
realizar cualquier otro cambio.

Al realizar la proyección inicial la cantidad de naranjas eran insuficientes,
ya que al finalizar el mes nos faltaban más de 2 kilos.
Para evitar este tipo de errores podemos incluir un pequeño indicador que nos
señale si tenemos capacidad de producción, revisando si las cantidades finales
son positivas.
Si algún producto en inventario termina en un número menor a cero, mostraremos
la palabra “ERROR” en la celda de check, y si todas las cantidades son positivas
presentaremos “OK”.

![Formula check
inventarios](curso-de-finanzas-primer-modelo/formula-check-inventarios.png)

Con esta verificación es posible realizar los ajustes necesarios, ya sea en las
proyecciones de ventas o en las de compras.
Este check también puede incluirse en los otros reportes.
Para ello, solo es necesario referenciar hacia los inventarios desde la sección
que corresponde.
Podemos incluir otro tipo de chequeos que nos permiten saber si debemos ajustar
alguna proyección sin tener que estar revisando todas las hojas de forma
individual.
Esto reduce la posibilidad de que cometamos un error.

![Check inventarios](curso-de-finanzas-primer-modelo/check-inventarios.png)

Hasta este punto tenemos un análisis simple de nuestro puesto.
Antes de continuar mejorándolo sería interesante jugar con él.
Usemos un ejemplo sencillo: vendemos 50 vasos chicos y 30 grandes durante 20
días al mes (descansando fines de semana).
Bajo estas condiciones
¿cuál es el comportamiento del negocio?
En lo que se refiere a los gastos y la inversión necesaria para operar
¿cuántos insumos debemos adquirir?
¿cómo podemos optimizar las compras?

Si fijamos la atención del lado de las ganancias generadas
¿cuánto venderemos cada mes?
¿cuál es la utilidad bruta?
¿qué montos se registran en la hoja de compras y en el apartado de costo de
productos vendidos?
¿cómo se explica esta diferencia?

La última pregunta es muy interesante.
En el reporte de ventas calculamos cuánto cuesta producir todos los jugos
vendidos, sin embargo en la hoja de compras el gasto es mayor.
Esto se debe a que, como dijimos en un principio, el costo de producción
considera el precio de cada elemento individual pero compramos paquetes.
Al finalizar el mes terminamos con material que no utilizamos.
La lección a aprender en que no todas las adquisiciones que realicemos se
traducen en un beneficio para el negocio.

Otra forma de verlo es que este material que queda guardado en el almacén se
puede ocuparlo en el siguiente mes.
Conforme pasa el tiempo somos capaces de acumular suficientes excedentes como
para dejar de comprar paquetes nuevos.
De esta manera, después de algunos meses ocupamos lo que tenemos en vez de
adquirir más.

Si no logramos convertir lo que hay en los almacenes en productos y venderlos,
las compras terminarán convirtiéndose en un gasto en lugar de ser una
inversión.
Por esta razón, debemos estimar la cantidad de material que vamos a necesitar
durante la producción.
Es mejor comprar pocos insumos varias veces a adquirir mucho una sola vez y no
poder sacar beneficio después.

Realizando un seguimiento a nuestros procesos con un modelo similar a este y
entendemos la dinámica del negocio, podemos comprar de forma inteligente.
Al conocer el volumen de ventas que hemos tenido, seremos capaces de realizar
proyecciones más confiables a futuro y determinar las compras necesarias para
mantener el ritmo.
Con esta información en la mano es posible incrementar las ganancias con
estrategias como buscar mejores proveedores, adquirir productos por mayoreo,
optimizar los sistemas de producción, entre otras.

<iframe
  width="100%"
  height="500"
  style="border:1px solid #ccc"
  frameborder="0"
  scrolling="no"
  src="https://sheet.zoho.com/sheet/published.do?rid=keoms4356550c1ad442359198b0be49d548a5&mode=embed"
></iframe>
