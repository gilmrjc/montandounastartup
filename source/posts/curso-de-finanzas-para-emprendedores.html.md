---

title: Curso de finanzas para emprendedores
description: >-
  Antes de emprender es vital tener dos cosas en claro: a quien le vas a vender
  y cuanto vas a ganar con cada venta. Todos sabemos que un negocio que no vende
  no es negocio, y nadie quiere perder dinero. El objetivo de este curso es
  aprender a crear un modelo financiero y llevar el control de las finanzas en
  una empresa.
date: 2020-04-15
modified: 2020-04-21
cover: /posts/curso-de-finanzas-para-emprendedores/cover.jpg
pull_image: curso-de-finanzas-para-emprendedores/cover.jpg

---

Antes de emprender es vital tener dos cosas en claro: a quien le vas a vender y
cuanto vas a ganar con cada venta.
Todos sabemos que un negocio que no vende no es negocio, y nadie quiere perder
dinero.
El objetivo de este curso es aprender a crear un modelo financiero y llevar el
control de las finanzas en una empresa.

**Nota**: Este curso está dividido en varias partes y sigue en proceso de
desarrollo. Aquí se irán agregando las distintas partes en cuanto se publiquen.

[Parte I: Primer modelo](posts/curso-de-finanzas-primer-modelo)

## ¿Qué temas se van a estudiar?

A grandes rasgos, la finalidad del curso es explicar el funcionamiento de un
negocio desde el punto de vista financiero.
Para ello, revisaremos la forma en que nos afecta cada decisión relacionada con
el uso del dinero.
Una gran herramienta que nos permite realizar este tipo de análisis y con la que
abordaremos todos los temas es un modelo financiero.
Una vez explicado que son y como se construyen, realizaremos cuatro ejemplos.
Con ellos se expondrán los distintos conceptos que integran el control
financiero de una empresa.

El primero simula un negocio pequeño.
Este nos servirá para entender como se organiza la información y los conceptos
básicos que nos ayudarán a llevar el control financiero de una empresa.
Durante su creación iremos incorporando los datos necesarios con los que se
generan las proyecciones de ganancias y gastos.
Al hacerlo de forma gradual veremos la relación que existe entre las distintas
variables conforme las vamos agregando.

El objetivo principal es observar como cambian nuestros planes dependiendo de la
cantidad de información que tenemos disponible.
Es fundamental entender que un modelo financiero es una herramienta que nos
ayuda a tomar decisiones y su utilidad depende de nuestra capacidad para incluir
todos los datos relevante.

El segundo simula un negocio de ventas por internet.
A este ejemplo le agregaremos un sistema de control que nos permite combinar las
proyecciones con la operación.
Esto lo haremos para comparar nuestras expectativas iniciales con los números
reales.
Una ventaja que nos brinda el cambio en la forma de presentar la información es
establecer los cálculos como objetivos a cumplir.

Otra ventaja de tener ambos datos en el mismo lugar es que cuando no cubrimos
nuestras metas podemos volver a calcular los pronósticos con los números reales
y así tomar decisiones para ajustarnos a la situación actual.
Al actualizar la información mejoramos de forma continua nuestros modelos, lo
que nos facilita entender que sucede.
Todo este conocimiento es útil al establecer estrategias de corto y largo plazo,
ya que nos brinda un alto grado de confianza en las predicciones.

El tercero se centrará en las finanzas personales.
Con los anteriores es posible determinar cuanto dinero se requiere para comenzar
un negocio.
Sin embargo, la gran incógnita es ¿de dónde saldrá la inversión inicial?
Suponiendo que nosotros la aportaremos, necesitamos un plan financiero personal
que nos indique como reunir la cantidad necesaria y el tiempo que tardaremos.

Incluso es posible extenderlo para incluir otros objetivos como comprar un
coche, organizar un viaje o realizar otro tipo de inversiones.
Un modelo financiero personal es una gran herramienta que nos ayuda a determinar
la capacidad que tenemos de llevar a cabo proyectos y ser realistas con nuestras
metas.
Estos ejercicios son los que nos permiten lograr la libertad financiera.

Por último haremos el modelo financiero de una empresa tecnológica.
El cambio más notable es que este tipo de negocios no generan productos físicos
sino servicios.
Esto elimina los gastos de producción y hace que nos centremos con mayor detalle
en los de administración.
Además, para modelar un negocio tecnológico es necesario incluir el
comportamiento de los clientes en el análisis.
De esta manera, el enfoque estará en las ventas y la adquisición de usuarios, y
en entender la dinámica que existe entre los esfuerzos de publicidad con los
ingresos.
Una vez que quede claro como se relacionan podemos realizar el mismo proceso en
los ejemplos anteriores.

## ¿Qué es un modelo financiero?

De forma simple, es una representación matemática para determinar el
funcionamiento una empresa y cuál será su evolución en el tiempo.
Al construirlo se toman en varios supuestos iniciales, como el volumen de ventas
o la cantidad de materia prima necesaria, y con ellos se calculan los posibles
resultados, que pueden ser las ganancias o los costos de producción.
Modelar un negocio permite entender mejor los distintos escenarios, ya que es
posible “jugar con los números” y simular que sucedería si se contrata a más
personal, si se cambia la estrategia de publicidad, si se hace una expansión con
nuevas sucursales, o cualquier otra decisión.
A menudo se realiza en una hoja de cálculo electrónica.

Una de las principales razones para elaborar un modelo financiero antes de
comenzar un negocio es tener claro cuales son los costos y posibles ganancias.
Entender estos números nos permite detectar áreas de oportunidad y crear
estrategias prácticas.
Más allá de un tema monetario, se vuelve una herramienta de planeación
estratégica fundamental en cualquier emprendimiento.

Aunque existen plantillas en el Internet que modelan distintos tipos de
negocios, es conveniente aprender a crearlos desde cero.
Cada uno utiliza sus propios supuestos iniciales y es posible que asuman
condiciones que no coinciden con nuestro entorno.
Debido a estas diferencias podemos terminar usando un modelo que no refleje la
realidad y genere resultados poco útiles.

El principal problema que ocurre al utilizar una plantilla es la dificultad de
actualizar el modelo.
Al detectar una diferencia entre la información que contiene y nuestros
requisitos puede resultar muy complejo cambiarlo.
Otro inconveniente es que es posible que haya partes que sean difíciles de
entender y no queda claro el significado de esos datos.

## ¿Cuáles son los componentes de un modelo financiero?

La información que integra un modelo incluye los reportes de ventas, compras,
manejo de inventarios, nómina, etc.
Cualquier dato que involucre o influya en los movimientos de dinero puede
utilizarse.
La decisión respecto a que incluir depende del nivel de detalle con el que lo
construyamos.
Lo mejor es armarlo de forma gradual y enfocarnos en un tema a la vez.
Esto nos permite entender la relación entre las distintas partes.

Como habíamos mencionado antes, todos los modelos se basan en varios supuestos
iniciales que se utilizan para proyectar el comportamiento de la empresa a lo
largo del tiempo.
Agruparlos en una sección propia permite actualizar los cálculos realizando
cambios en un solo lugar.
La información dependen del tipo de negocio a analizar pero suele incluir, por
ejemplo, el costo de las materias primas, servicios o productos vendidos.
De esta forma, se construye usando de base los componentes más pequeños.

A diferencia de los cálculos que se realizarán en otras partes del modelo, los
supuestos iniciales no depende de ninguna fórmula matemática y no pueden ser
verificados, por lo que es muy fácil introducir datos incorrectos sin darnos
cuenta.
Es necesario revisar que coincidan con las condiciones externas y comparar
distintas fuentes para asegurarnos que estamos siendo realistas.

Cuando se investigue se debe entender el contexto en el que nos encontramos.
Es muy fácil presupuestar una campaña publicitaria con personalidades de
Internet pero esa no es una buena idea.
Un error común que cometen muchas empresas es gastar en publicidad antes de
generar ganancias.
Al carecer de un ingreso fijo terminan por quebrar al utilizar el poco capital
que tienen en actividades que no son productivas.

Aparte de estos datos, existen tres reportes llamados estados financieros: el
balance general, el estado de resultados y el estado de flujos de caja.
Cada uno debe de tener su propia sección.
En ellos se resume toda la información y nos ayudan a entender como se
encuentran las finanzas del negocio.

El balance general representa la situación actual de la empresa.
Entre los elementos que lo conforman se encuentran los costos de producción y
las ventas realizadas.
En el mismo reporte se incluyen las deudas que tenemos y que nos deben.
Otro registro que se puede encontrar son las aportaciones de capital que han
hecho los socios.

El estado de resultados, también llamado estado de pérdidas y ganancias, muestra
los ingresos y gastos que ha tenido la empresa en un periodo específico.
Nos permite entender la situación financiera en términos de rentabilidad.
Si el balance general se puede considerar como una fotografía completa de las
finanzas, este reporte se enfoca en el rendimiento de las actividades
realizadas.

Por último se encuentra el estado de flujos de caja que registra los movimientos
del efectivo.
Determinarlo es necesario porque aunque el balance general y el estado de
resultados pueden incluir ventas que aún no han sido cobradas, el flujo de caja
solo cambia cuando hay movimiento real de dinero.
Este documento nos permite entender el nivel de liquidez que tenemos.
Con todos estos datos podemos determinar la situación de nuestro negocio.
