---

title: Mejorar la web
description: >-
  A partir de los primeros comentarios recibidos sobre este sitio, se ha
  decidido mejorar el diseño de la web. El objetivo principal de estas mejoras
  es reducir la cantidad de comentarios iniciales que se centran en el aspecto
  del sitio en lugar del contenido.
date: 2020-01-03
deadline: 2020-01-24
type: Implementación
version: 1

---

## Descripción

A partir de los primeros comentarios recibidos sobre este sitio, se ha decidido
mejorar el diseño de la web.
Los principales a mejorar problemas son:

- Falta de un menú de navegación.
- Falta de un logo.
- Carencia de una estructura familiar (cabecera, contenido, pie de página).
- Mejor presentación del contenido a explorar.

El objetivo principal de estas mejoras es reducir la cantidad de comentarios
iniciales que se centran en el aspecto del sitio en lugar del contenido.
Una vez aplicados estos cambios se espera una mejor respuesta durante la
primera visita.
En caso de seguir recibiendo comentarios sobre la apariencia se deberá realizar
una investigación para determinar porque continua siendo la sugerencia más
recurrente.


## Contexto

Para validar [la idea central de este proyecto][hola-mundo] decidi construir un
blog y publicar un par de artículos explicando las motivaciones que le dieron
origen.
El plan era compartirlo con algunas personas que pudieran estar interesadas en
el tema para recoger sus impresiones y verificar si les parecia buen contenido.

Aunque la intención era obtener comentarios de las publicaciones, muchos
hicieron sugerencias sobre el diseño.
Debo admitir que eso no fue algo inesperado.
Un [estudio sobre confianza en páginas web][estudio-confianza] demostró que el
diseño influye hasta en un 94% sobre la percepción de las personas respecto a la
confiabilidad de un sitio.

Las opiniones recabadas durante el experimento van más alla de *"este sitio
está feo"* o *"este otro sitio está bonito"*.
Los investigadores identificaron 10 problemas específicos relacionados con la
estructura y presentación del mismo.
El 6% restante se adjudicaba a *"contenido irrelevante o inapropiado"*.

Este estudio no solo le pone un número a la importancia del diseño, también
deja claro que es necesario tener un buen diseño si queremos que la gente se
fije en el contenido.

Previamente ya había [detectado este problema][mapa-problemas], sin embargo en
ese momento le di prioridad a la creación de contenido que a mejorar el diseño
de la página para poder validar la idea.
Los comentarios que se centraron en el contenido fueron positivos y mencionaron
algunos puntos que también se deben mejorar.
Una vez dicho lo anterior, y validado el interes sobre el tema, es momento de
trabajar en el diseño.

Para comenzar, necesitamos tener una referencia del diseño actual y cuales son
las críticas que recibió:

![Diseño inicial del sitio](mejorar-la-web/website-initial-design.png)

El diseño inicial es muy simple.
Solo contiene el título y la lista de artículos publicados.
Entre los principales problemas se encuentran la falta de un sistema de
navegación, la falta de una estructura familiar a los visitantes, el poco uso de
imágenes para enfatizar el contenido, el nulo contexto inicial y un mal uso de
la tipografía como recurso visual.

## Propuesta

Para mejorar el sitio web fácil y rápido me voy a centrar en los siguientes
puntos:

- Crear un menú de navigación.
- Mejorar la presentación inicial con una *hero image* en la cabecera.
- Agregar un pie de página.
- Agregar portadas a los diferentes artículos y mostrar en forma de cuadrículo.
  para que sean más fáciles de inspeccionar a primera vista.
- Cambiar la tipografía del sitio para mejorar la experiencia de lectura.

Los cambios antes mencionados solo mejorarán la página principal del sitio.
Las páginas internas se verán beneficiadas de forma marginal.
La adaptación de los artículos y las demás secciones se realizará durante una
segunda iteración.

## Métricas

Para establecer de forma correcta que está estrategia es efectiva, se deben de
modificar dos variables:

- Disminuir el número de comentarios relacionados con el diseño de la página
  de inicio a menos del 40%.
- Aumentar el número de comentarios sobre el contenido publicado hasta un 60%,
  complementario a la meta anterior.

Al carecer de un histórico público de los comentarios realizados, las métricas
considerarán opiniones sobre este cambio desde la fecha de publicación del nuevo
diseño hasta 20 días después.
En caso de que las opiniones hagan referencia a ambos aspectos, se considerará
un éxito si los comentarios no incluyen al diseño como primer elemento.

[hola-mundo]: /hola-mundo/
[estudio-confianza]: https://www.researchgate.net/publication/221516871_Trust_and_mistrust_of_online_health_sites
[mapa-problemas]: /primero-lo-primero/
